using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//[ExecuteAlways]
public class DialogueBoxFitter : MonoBehaviour
{
    [SerializeField] private RectTransform textTransform;

    public RectTransform _rectTransform;
    public RectTransform _parentTransform;

    public RectTransform rectTransform
    {
        get
        {
            if(_rectTransform == null)
                _rectTransform = transform.GetComponent<RectTransform>();

            return _rectTransform;
        }
    }

    public RectTransform parentTransform
    {
        get
        {
            if(_parentTransform == null)
                _parentTransform = _rectTransform.parent.parent.GetComponent<RectTransform>();

            return _parentTransform;
        }
    }

    void OnRectTransformDimensionsChange()
    {
        LayoutRebuilder.ForceRebuildLayoutImmediate(parentTransform);
    }

    void LateUpdate()
    {
        //yield return new WaitForEndOfFrame();
        Vector2 sizeDelta = rectTransform.sizeDelta;
        sizeDelta.y = textTransform.sizeDelta.y;
        rectTransform.sizeDelta = sizeDelta;
    }
}
