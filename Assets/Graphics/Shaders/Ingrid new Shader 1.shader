// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Ingrid Shader"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		_Timeline("_Timeline", Range( 0 , 1)) = 0.4718255
		_MainTex("Main Tex", 2D) = "white" {}
		_TopTexture1("Top Texture 1", 2D) = "white" {}
		_OffsetX("Offset X", Range( 0 , 1)) = 0.018
		_OffsetY("Offset Y", Range( 0 , 1)) = 0.018
		_NoiseScale("Noise Scale", Range( 0 , 100)) = 36.67862
		_NoiseScale2("Noise Scale 2", Range( 0 , 100)) = 5
		_TimeScaleBig("TimeScaleBig", Float) = 1
		_UpperLimit("UpperLimit", Range( 0 , 2)) = 0.55
		_TimeScaleSmall("TimeScaleSmall", Float) = 1
		_LowerLimit("LowerLimit", Range( -1 , 1)) = 0.3176471
		_Float0("Float 0", Range( 0 , 10)) = 1
		_Float5("Float 5", Range( 0 , 2)) = 2
		_Float6("Float 6", Range( 0 , 2)) = 1
		_TilingMultiplierDissolve("Tiling Multiplier Dissolve", Float) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "AlphaTest+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		ZTest Always
		Blend SrcAlpha OneMinusSrcAlpha
		
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 5.0
		#pragma surface surf Unlit keepalpha noshadow 
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
			float3 worldNormal;
			INTERNAL_DATA
		};

		uniform sampler2D _MainTex;
		uniform float4 _MainTex_ST;
		uniform float _OffsetX;
		uniform float _OffsetY;
		uniform float _TimeScaleSmall;
		uniform float _NoiseScale;
		uniform float _TimeScaleBig;
		uniform float _NoiseScale2;
		sampler2D _TopTexture1;
		uniform float _Float6;
		uniform float _Float5;
		uniform float _Timeline;
		uniform float _TilingMultiplierDissolve;
		uniform float _Float0;
		uniform float _LowerLimit;
		uniform float _UpperLimit;
		uniform float _Cutoff = 0.5;


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		inline float4 TriplanarSampling200( sampler2D topTexMap, float3 worldPos, float3 worldNormal, float falloff, float2 tiling, float3 normalScale, float3 index )
		{
			float3 projNormal = ( pow( abs( worldNormal ), falloff ) );
			projNormal /= ( projNormal.x + projNormal.y + projNormal.z ) + 0.00001;
			float3 nsign = sign( worldNormal );
			half4 xNorm; half4 yNorm; half4 zNorm;
			xNorm = tex2D( topTexMap, tiling * worldPos.zy * float2(  nsign.x, 1.0 ) );
			yNorm = tex2D( topTexMap, tiling * worldPos.xz * float2(  nsign.y, 1.0 ) );
			zNorm = tex2D( topTexMap, tiling * worldPos.xy * float2( -nsign.z, 1.0 ) );
			return xNorm * projNormal.x + yNorm * projNormal.y + zNorm * projNormal.z;
		}


		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			o.Normal = float3(0,0,1);
			float2 uv_MainTex = i.uv_texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
			float4 Diffuse137 = tex2D( _MainTex, uv_MainTex );
			float4 RimlightDiffuse220 = Diffuse137;
			float4 temp_output_228_0 = RimlightDiffuse220;
			o.Emission = temp_output_228_0.rgb;
			o.Alpha = 1;
			float OffsetX66 = _OffsetX;
			float2 appendResult124 = (float2(OffsetX66 , 0.0));
			float2 appendResult123 = (float2(-OffsetX66 , 0.0));
			float OffsetY171 = _OffsetY;
			float2 appendResult122 = (float2(0.0 , OffsetY171));
			float2 appendResult119 = (float2(0.0 , -OffsetY171));
			float clampResult141 = clamp( ( tex2D( _MainTex, ( i.uv_texcoord + appendResult124 ) ).a + tex2D( _MainTex, ( i.uv_texcoord + appendResult123 ) ).a + tex2D( _MainTex, ( i.uv_texcoord + appendResult122 ) ).a + tex2D( _MainTex, ( i.uv_texcoord + appendResult119 ) ).a ) , 0.0 , 1.0 );
			float clampResult148 = clamp( pow( clampResult141 , 5.0 ) , 0.0 , 1.0 );
			float mulTime169 = _Time.y * _TimeScaleSmall;
			float2 appendResult167 = (float2(0.0 , mulTime169));
			float2 uv_TexCoord156 = i.uv_texcoord + appendResult167;
			float simplePerlin2D153 = snoise( uv_TexCoord156*_NoiseScale );
			simplePerlin2D153 = simplePerlin2D153*0.5 + 0.5;
			float mulTime165 = _Time.y * _TimeScaleBig;
			float2 appendResult164 = (float2(0.0 , mulTime165));
			float2 uv_TexCoord157 = i.uv_texcoord + appendResult164;
			float simplePerlin2D159 = snoise( uv_TexCoord157*_NoiseScale2 );
			simplePerlin2D159 = simplePerlin2D159*0.5 + 0.5;
			float Edgeglow128 = ( clampResult148 * ( simplePerlin2D153 * simplePerlin2D159 ) );
			float clampResult149 = clamp( ( Diffuse137.a + Edgeglow128 ) , 0.0 , 1.0 );
			float RimlightAlpha223 = clampResult149;
			float _Timeline177 = _Timeline;
			float temp_output_190_0 = ( _Float5 * ( ( 1.0 - _Timeline177 ) * i.uv_texcoord.y ) );
			float4 appendResult196 = (float4(_Float6 , ( 1.0 - temp_output_190_0 ) , 0.0 , 0.0));
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float temp_output_184_0 = ( ( ( i.uv_texcoord.y + 0.0 ) * _Timeline177 ) * _Float0 );
			float2 appendResult186 = (float2(temp_output_184_0 , temp_output_184_0));
			float2 DistortEffect191 = ( i.uv_texcoord + appendResult186 );
			float2 break243 = DistortEffect191;
			float3 appendResult244 = (float3(break243.x , break243.y , break243.x));
			float4 triplanar200 = TriplanarSampling200( _TopTexture1, appendResult244, ase_worldNormal, 1.0, ( appendResult196 * _TilingMultiplierDissolve ).xy, 1.0, 0 );
			float4 temp_output_205_0 = ( ( triplanar200 * ase_worldPos.y ) + (_LowerLimit + (_Timeline177 - 0.0) * (_UpperLimit - _LowerLimit) / (1.0 - 0.0)) );
			float4 clampResult235 = clamp( ( RimlightAlpha223 * temp_output_205_0 ) , float4( 0,0,0,0 ) , float4( 1,1,1,1 ) );
			float4 DissolveAlpha225 = clampResult235;
			clip( DissolveAlpha225.x - _Cutoff );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18910
370.4;922.4;1378.4;703;4092.117;-1858.32;1;True;False
Node;AmplifyShaderEditor.RangedFloatNode;64;-4819.596,2531.414;Inherit;False;Property;_OffsetX;Offset X;4;0;Create;True;0;0;0;False;0;False;0.018;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;170;-4817.727,2676.624;Inherit;False;Property;_OffsetY;Offset Y;5;0;Create;True;0;0;0;False;0;False;0.018;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;66;-4372.305,2614.938;Inherit;False;OffsetX;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;171;-4373.153,2764.224;Inherit;False;OffsetY;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;120;-4004.603,2640.547;Inherit;False;66;OffsetX;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;172;-4000.654,2789.984;Inherit;False;171;OffsetY;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.NegateNode;174;-3442.114,2913.171;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NegateNode;126;-3506.107,2562.508;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;124;-3206.945,2324.45;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;116;-3633.754,2650.929;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;122;-3143.248,2702.882;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;176;-7275.746,192.4675;Inherit;False;Property;_Timeline;_Timeline;1;0;Create;True;0;0;0;False;0;False;0.4718255;0.999;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;123;-3163.856,2534.275;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.CommentaryNode;175;-6304.409,-928.9882;Inherit;False;1698.068;585.0474;Distort;9;189;186;184;182;181;180;179;178;242;;1,1,1,1;0;0
Node;AmplifyShaderEditor.DynamicAppendNode;119;-3051.702,2962.031;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;166;-1913.876,3688.565;Inherit;False;Property;_TimeScaleBig;TimeScaleBig;9;0;Create;True;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TexCoordVertexDataNode;178;-6268.782,-891.2795;Inherit;False;0;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;177;-6935.345,271.1713;Inherit;False;_Timeline;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;108;-2910.769,2295.944;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TexturePropertyNode;117;-3356.137,2120.242;Inherit;True;Property;_MainTex;Main Tex;2;0;Create;True;0;0;0;False;0;False;6d1802e6da0886c48bf8dee786fbfea4;cb1c9831a97b5264ebe3ca4847d009ea;False;white;Auto;Texture2D;-1;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.SimpleAddOpNode;111;-2830.041,2958.589;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;168;-1889.339,3406.32;Inherit;False;Property;_TimeScaleSmall;TimeScaleSmall;11;0;Create;True;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;109;-2867.041,2524.675;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;110;-2937.582,2689.495;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;105;-2729.215,2477.691;Inherit;True;Property;_TextureSample7;Texture Sample 7;9;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleTimeNode;165;-1702.561,3546.279;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;101;-2723.229,2687.213;Inherit;True;Property;_TextureSample3;Texture Sample 3;5;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleTimeNode;169;-1678.024,3264.034;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;106;-2658.502,2240.703;Inherit;True;Property;_TextureSample8;Texture Sample 8;10;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;104;-2690.303,2923.673;Inherit;True;Property;_TextureSample6;Texture Sample 6;8;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;180;-5906.96,-717.2573;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;179;-5916.128,-454.0129;Inherit;False;177;_Timeline;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;182;-5571.304,-690.3245;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;181;-5580.748,-462.8782;Inherit;False;Property;_Float0;Float 0;13;0;Create;True;0;0;0;False;0;False;1;0.1;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;167;-1548.542,3063.823;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;138;-2090.484,2839.329;Inherit;True;4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;164;-1573.079,3346.068;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;156;-1300.604,2997.415;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;154;-1269.486,3133.993;Inherit;False;Property;_NoiseScale;Noise Scale;6;0;Create;True;0;0;0;False;0;False;36.67862;0;0;100;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;157;-1321.663,3286.556;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;158;-1290.545,3423.134;Inherit;False;Property;_NoiseScale2;Noise Scale 2;7;0;Create;True;0;0;0;False;0;False;5;0;0;100;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;184;-5324.222,-706.9808;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;141;-1834.956,2872.439;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;186;-5103.651,-756.0342;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.OneMinusNode;183;-6673.704,300.8997;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;146;-992.2781,2754.913;Inherit;True;False;2;0;FLOAT;0;False;1;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;159;-939.726,3359.942;Inherit;True;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TexCoordVertexDataNode;185;-7230.681,367.491;Inherit;False;0;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NoiseGeneratorNode;153;-918.6665,3070.801;Inherit;True;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;188;-6477.695,354.0181;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;161;-595.7493,3117.779;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;189;-4841.34,-878.9882;Inherit;True;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;69;-2742.994,1982.511;Inherit;True;Property;_TextureSample2;Texture Sample 2;1;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ClampOpNode;148;-735.8909,2749.689;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;187;-6574.541,203.0565;Inherit;False;Property;_Float5;Float 5;14;0;Create;True;0;0;0;False;0;False;2;2;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;155;-485.2881,2775.083;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;191;-4531.741,-856.4318;Inherit;False;DistortEffect;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;137;-1693.763,2212.132;Inherit;False;Diffuse;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;190;-6170.323,210.0206;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;194;-5425.704,-294.5526;Inherit;False;191;DistortEffect;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;193;-5927.504,-18.02924;Inherit;False;Property;_Float6;Float 6;15;0;Create;True;0;0;0;False;0;False;1;1;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;132;-2297.679,68.65408;Inherit;False;137;Diffuse;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;128;-295.2935,2716.344;Inherit;True;Edgeglow;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;192;-5698.544,162.171;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;196;-5452.359,52.26563;Inherit;True;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;129;-2022.586,660.2667;Inherit;True;128;Edgeglow;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;136;-1668.796,245.6089;Inherit;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.RangedFloatNode;240;-5199.278,162.1273;Inherit;False;Property;_TilingMultiplierDissolve;Tiling Multiplier Dissolve;17;0;Create;True;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;243;-5159.041,-292.2149;Inherit;False;FLOAT2;1;0;FLOAT2;0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.DynamicAppendNode;244;-4934.094,-252.3665;Inherit;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;239;-5070.278,-55.87271;Inherit;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleAddOpNode;143;-1631.488,663.4792;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;149;-1397.103,675.0009;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;201;-4135.076,-22.26184;Inherit;False;Property;_LowerLimit;LowerLimit;12;0;Create;True;0;0;0;False;0;False;0.3176471;0.3176471;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TriplanarNode;200;-4762.481,-221.0331;Inherit;True;Spherical;World;False;Top Texture 1;_TopTexture1;white;3;None;Mid Texture 1;_MidTexture1;white;-1;None;Bot Texture 1;_BotTexture1;white;-1;None;Triplanar Sampler;Tangent;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WorldPosInputsNode;246;-4250.686,-774.1774;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.GetLocalVarNode;197;-4200.548,-119.0242;Inherit;False;177;_Timeline;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;198;-4092.177,58.58856;Inherit;False;Property;_UpperLimit;UpperLimit;10;0;Create;True;0;0;0;False;0;False;0.55;2;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;203;-3763.989,-83.90625;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;223;-896.0098,696.0438;Inherit;False;RimlightAlpha;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;202;-3698.157,-372.3687;Inherit;True;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleAddOpNode;205;-3448.845,-287.795;Inherit;True;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;226;-2533.931,-421.4644;Inherit;True;223;RimlightAlpha;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;213;-2070.454,-319.6852;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.ClampOpNode;235;-1796.541,-304.8369;Inherit;True;3;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT4;1,1,1,1;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;220;-827.7805,-26.40621;Inherit;False;RimlightDiffuse;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;225;-1504.128,-322.579;Inherit;False;DissolveAlpha;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.ColorNode;52;-1980.297,397.7775;Inherit;False;Constant;_Color0;Color 0;3;1;[HDR];Create;True;0;0;0;False;0;False;0,0.3080587,1.898039,1;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;237;-2559.261,-705.2354;Inherit;False;220;RimlightDiffuse;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;150;-1530.059,415.1703;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;204;-3268.567,-547.5995;Inherit;False;Property;_ColorRimlight;Color Rimlight;8;0;Create;True;0;0;0;False;0;False;0,0,0,1;0,0,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;151;-1210.687,499.8847;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;221;-940.2877,476.1816;Inherit;True;RimlightEmission;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;238;-2099.54,-780.8871;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;231;-135.2646,227.8064;Inherit;True;221;RimlightEmission;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;210;-2000.996,-431.3207;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;230;-269.7647,447.6064;Inherit;False;225;DissolveAlpha;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WorldPosInputsNode;242;-6251.984,-770.5125;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.TimeNode;163;-2396.343,3321.073;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;214;-6222.04,504.8718;Inherit;False;Property;_Float7;Float 7;16;0;Create;True;0;0;0;False;0;False;0.1117433;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;217;-5895.882,247.2914;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;224;-1481.244,-683.9682;Inherit;False;DissolveDiffuse;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;207;-2194.713,-479.7397;Inherit;False;177;_Timeline;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;211;-2181.958,-681.3651;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;245;63.03467,117.0726;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;206;-2792.334,-473.1194;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;216;-1880.107,-668.7269;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;145;-1461.244,33.63625;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;228;-285.2953,31.67483;Inherit;True;220;RimlightDiffuse;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;358.6695,41.91578;Float;False;True;-1;7;ASEMaterialInspector;0;0;Unlit;Ingrid Shader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;7;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;False;0;True;Transparent;;AlphaTest;All;16;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;66;0;64;0
WireConnection;171;0;170;0
WireConnection;174;0;172;0
WireConnection;126;0;120;0
WireConnection;124;0;120;0
WireConnection;122;1;172;0
WireConnection;123;0;126;0
WireConnection;119;1;174;0
WireConnection;177;0;176;0
WireConnection;108;0;116;0
WireConnection;108;1;124;0
WireConnection;111;0;116;0
WireConnection;111;1;119;0
WireConnection;109;0;116;0
WireConnection;109;1;123;0
WireConnection;110;0;116;0
WireConnection;110;1;122;0
WireConnection;105;0;117;0
WireConnection;105;1;109;0
WireConnection;165;0;166;0
WireConnection;101;0;117;0
WireConnection;101;1;110;0
WireConnection;169;0;168;0
WireConnection;106;0;117;0
WireConnection;106;1;108;0
WireConnection;104;0;117;0
WireConnection;104;1;111;0
WireConnection;180;0;178;2
WireConnection;182;0;180;0
WireConnection;182;1;179;0
WireConnection;167;1;169;0
WireConnection;138;0;106;4
WireConnection;138;1;105;4
WireConnection;138;2;101;4
WireConnection;138;3;104;4
WireConnection;164;1;165;0
WireConnection;156;1;167;0
WireConnection;157;1;164;0
WireConnection;184;0;182;0
WireConnection;184;1;181;0
WireConnection;141;0;138;0
WireConnection;186;0;184;0
WireConnection;186;1;184;0
WireConnection;183;0;177;0
WireConnection;146;0;141;0
WireConnection;159;0;157;0
WireConnection;159;1;158;0
WireConnection;153;0;156;0
WireConnection;153;1;154;0
WireConnection;188;0;183;0
WireConnection;188;1;185;2
WireConnection;161;0;153;0
WireConnection;161;1;159;0
WireConnection;189;0;178;0
WireConnection;189;1;186;0
WireConnection;69;0;117;0
WireConnection;148;0;146;0
WireConnection;155;0;148;0
WireConnection;155;1;161;0
WireConnection;191;0;189;0
WireConnection;137;0;69;0
WireConnection;190;0;187;0
WireConnection;190;1;188;0
WireConnection;128;0;155;0
WireConnection;192;0;190;0
WireConnection;196;0;193;0
WireConnection;196;1;192;0
WireConnection;136;0;132;0
WireConnection;243;0;194;0
WireConnection;244;0;243;0
WireConnection;244;1;243;1
WireConnection;244;2;243;0
WireConnection;239;0;196;0
WireConnection;239;1;240;0
WireConnection;143;0;136;3
WireConnection;143;1;129;0
WireConnection;149;0;143;0
WireConnection;200;9;244;0
WireConnection;200;3;239;0
WireConnection;203;0;197;0
WireConnection;203;3;201;0
WireConnection;203;4;198;0
WireConnection;223;0;149;0
WireConnection;202;0;200;0
WireConnection;202;1;246;2
WireConnection;205;0;202;0
WireConnection;205;1;203;0
WireConnection;213;0;226;0
WireConnection;213;1;205;0
WireConnection;235;0;213;0
WireConnection;220;0;132;0
WireConnection;225;0;235;0
WireConnection;150;0;136;3
WireConnection;151;0;150;0
WireConnection;151;1;52;0
WireConnection;221;0;151;0
WireConnection;238;0;237;0
WireConnection;210;0;207;0
WireConnection;217;0;190;0
WireConnection;217;1;214;0
WireConnection;224;0;216;0
WireConnection;211;0;206;0
WireConnection;211;1;226;0
WireConnection;245;0;228;0
WireConnection;245;1;231;0
WireConnection;206;0;204;0
WireConnection;206;1;205;0
WireConnection;216;0;238;0
WireConnection;216;1;211;0
WireConnection;216;2;210;0
WireConnection;145;0;52;0
WireConnection;145;2;136;3
WireConnection;0;2;228;0
WireConnection;0;10;230;0
ASEEND*/
//CHKSM=B4F343928AC91C4B4C758B86DA672404E26E395F