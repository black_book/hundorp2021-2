// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "BlackBook/Sprites/Dissolve"
{
	Properties
	{
		_Timeline("_Timeline", Range( 0 , 1)) = 0
		_GradientTightness("Gradient Tightness", Float) = 1
		[KeywordEnum(BottomToTop,TopToBottom)] _DissolveDirection("Dissolve Direction", Float) = 0
		_NoiseTexture("Noise Texture", 2D) = "white" {}
		[HideInInspector][NoScaleOffset]_MainTex("Main Texture", 2D) = "white" {}
		[NoScaleOffset]_DissolveMask("Dissolve Mask", 2D) = "white" {}
		_CutoffPosition("Cutoff Position", Range( 0 , 1)) = 0
		_CutoffTightness("Cutoff Tightness", Float) = 2
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Off
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma shader_feature_local _DISSOLVEDIRECTION_BOTTOMTOTOP _DISSOLVEDIRECTION_TOPTOBOTTOM
		#pragma surface surf Unlit alpha:fade keepalpha noshadow noambient novertexlights nolightmap  nodynlightmap nodirlightmap 
		struct Input
		{
			float4 vertexColor : COLOR;
			float2 uv_texcoord;
			float4 screenPos;
		};

		uniform sampler2D _MainTex;
		uniform sampler2D _DissolveMask;
		uniform float _CutoffTightness;
		uniform float _CutoffPosition;
		uniform float _GradientTightness;
		uniform float _Timeline;
		uniform sampler2D _NoiseTexture;
		uniform float4 _NoiseTexture_ST;
		float4 _NoiseTexture_TexelSize;

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float2 uv_MainTex1 = i.uv_texcoord;
			float4 break7 = ( i.vertexColor * tex2D( _MainTex, uv_MainTex1 ) );
			float3 appendResult3 = (float3(break7.r , break7.g , break7.b));
			o.Emission = appendResult3;
			float2 uv_DissolveMask32 = i.uv_texcoord;
			#if defined(_DISSOLVEDIRECTION_BOTTOMTOTOP)
				float staticSwitch60 = ( 1.0 - tex2D( _DissolveMask, uv_DissolveMask32 ).b );
			#elif defined(_DISSOLVEDIRECTION_TOPTOBOTTOM)
				float staticSwitch60 = tex2D( _DissolveMask, uv_DissolveMask32 ).b;
			#else
				float staticSwitch60 = ( 1.0 - tex2D( _DissolveMask, uv_DissolveMask32 ).b );
			#endif
			float temp_output_15_0_g5 = _CutoffTightness;
			float temp_output_15_0_g4 = _GradientTightness;
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float4 break24 = ( ase_screenPosNorm * _ScreenParams * _NoiseTexture_TexelSize );
			float2 appendResult26 = (float2(break24.x , break24.y));
			float2 temp_output_28_0 = ( _NoiseTexture_ST.xy * ( _NoiseTexture_ST.zw + appendResult26 ) );
			clip( ( saturate( ( ( ( staticSwitch60 * temp_output_15_0_g5 ) - temp_output_15_0_g5 ) + ( temp_output_15_0_g5 * ( ( 1.0 / temp_output_15_0_g5 ) + 1.0 ) * _CutoffPosition ) ) ) * saturate( ( ( ( staticSwitch60 * temp_output_15_0_g4 ) - temp_output_15_0_g4 ) + ( temp_output_15_0_g4 * ( ( 1.0 / temp_output_15_0_g4 ) + 1.0 ) * _Timeline ) ) ) * ( _Timeline + ( ( tex2D( _NoiseTexture, temp_output_28_0 ).r + tex2D( _NoiseTexture, ( temp_output_28_0 * 0.5 ) ).r ) * 0.5 ) ) ) - 0.5);
			o.Alpha = break7.a;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18910
526;1017;2771;1102;2265.838;160.6447;1.726045;True;False
Node;AmplifyShaderEditor.CommentaryNode;19;-2092.702,1189.207;Inherit;False;1867.784;613.1669;Comment;15;29;28;27;26;25;24;23;22;20;21;71;73;72;74;75;Screen Space Noise;1,1,1,1;0;0
Node;AmplifyShaderEditor.ScreenParams;21;-2016.96,1411.907;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenPosInputsNode;20;-2030.06,1239.207;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexelSizeNode;22;-2042.702,1582.375;Inherit;False;29;1;0;SAMPLER2D;;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;23;-1763.691,1412.48;Inherit;False;3;3;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.BreakToComponentsNode;24;-1621.949,1412.483;Inherit;False;FLOAT4;1;0;FLOAT4;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.DynamicAppendNode;26;-1495.752,1412.753;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureTransformNode;25;-1557.535,1262.544;Inherit;False;29;False;1;0;SAMPLER2D;;False;2;FLOAT2;0;FLOAT2;1
Node;AmplifyShaderEditor.SimpleAddOpNode;27;-1320.052,1351.953;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;28;-1180.115,1267.251;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;74;-1177.625,1553.278;Inherit;False;Constant;_Float0;Float 0;6;0;Create;True;0;0;0;False;0;False;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;69;-1315.021,697.0213;Inherit;False;2113.8;465.8999;;9;35;30;68;31;60;61;63;32;90;Dissolve effect;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;72;-1000.625,1476.278;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0.25;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;32;-1286.533,761.4809;Inherit;True;Property;_DissolveMask;Dissolve Mask;5;1;[NoScaleOffset];Create;True;0;0;0;False;0;False;-1;None;e1a392f0da4351144a03076b854fae41;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;71;-837.625,1449.278;Inherit;True;Property;_TextureSample0;Texture Sample 0;3;0;Create;True;0;0;0;False;0;False;29;27bc543b9fe328a4288f1b271db1dd0f;27bc543b9fe328a4288f1b271db1dd0f;True;0;False;white;Auto;False;Instance;29;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RelayNode;63;-956.4537,855.0998;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;29;-838.2176,1239.38;Inherit;True;Property;_NoiseTexture;Noise Texture;3;0;Create;True;0;0;0;False;0;False;29;27bc543b9fe328a4288f1b271db1dd0f;27bc543b9fe328a4288f1b271db1dd0f;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;75;-513.625,1270.278;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;70;-514.6026,-236.7022;Inherit;False;852.2999;442.6;;5;3;7;6;1;5;Color;1,1,1,1;0;0
Node;AmplifyShaderEditor.OneMinusNode;61;-810.9852,789.4175;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;89;-743.6115,401.5807;Inherit;False;762.0099;260.7897;Cutoff Gradient;3;81;82;88;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;81;-708.886,567.2576;Inherit;False;Property;_CutoffPosition;Cutoff Position;6;0;Create;True;0;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;73;-371.625,1269.278;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;82;-610.886,480.2576;Inherit;False;Property;_CutoffTightness;Cutoff Tightness;7;0;Create;True;0;0;0;False;0;False;2;2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;1;-489.127,8.655518;Inherit;True;Property;_MainTex;Main Texture;4;2;[HideInInspector];[NoScaleOffset];Create;False;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.VertexColorNode;5;-365.327,-174.0443;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;35;-552.9744,942.5815;Inherit;False;Property;_GradientTightness;Gradient Tightness;1;0;Create;True;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;60;-629.9851,824.4177;Inherit;False;Property;_DissolveDirection;Dissolve Direction;2;0;Create;True;0;0;0;False;0;False;0;0;1;True;;KeywordEnum;2;BottomToTop;TopToBottom;Create;True;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;31;-614.4044,1032.926;Inherit;False;Property;_Timeline;_Timeline;0;0;Create;True;0;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;87;-292.5435,897.8318;Inherit;False;AnimateGradient;-1;;4;1599e169f352ece4c88e8b31e4850294;0;3;14;FLOAT;0;False;15;FLOAT;0;False;16;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;88;-278.886,523.2576;Inherit;False;AnimateGradient;-1;;5;1599e169f352ece4c88e8b31e4850294;0;3;14;FLOAT;0;False;15;FLOAT;0;False;16;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;68;-133.8539,1036.762;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;6;-121.427,-49.34433;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.BreakToComponentsNode;7;35.57307,-49.34433;Inherit;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;90;104.3384,872.4728;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClipNode;30;573.814,754.2778;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;3;172.573,-50.34433;Inherit;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;854.2996,-78.4819;Float;False;True;-1;2;ASEMaterialInspector;0;0;Unlit;BlackBook/Sprites/Dissolve;False;False;False;False;True;True;True;True;True;False;False;False;False;False;True;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.5;True;False;0;False;Transparent;;Transparent;All;16;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;23;0;20;0
WireConnection;23;1;21;0
WireConnection;23;2;22;0
WireConnection;24;0;23;0
WireConnection;26;0;24;0
WireConnection;26;1;24;1
WireConnection;27;0;25;1
WireConnection;27;1;26;0
WireConnection;28;0;25;0
WireConnection;28;1;27;0
WireConnection;72;0;28;0
WireConnection;72;1;74;0
WireConnection;71;1;72;0
WireConnection;63;0;32;3
WireConnection;29;1;28;0
WireConnection;75;0;29;1
WireConnection;75;1;71;1
WireConnection;61;0;63;0
WireConnection;73;0;75;0
WireConnection;60;1;61;0
WireConnection;60;0;63;0
WireConnection;87;14;60;0
WireConnection;87;15;35;0
WireConnection;87;16;31;0
WireConnection;88;14;60;0
WireConnection;88;15;82;0
WireConnection;88;16;81;0
WireConnection;68;0;31;0
WireConnection;68;1;73;0
WireConnection;6;0;5;0
WireConnection;6;1;1;0
WireConnection;7;0;6;0
WireConnection;90;0;88;0
WireConnection;90;1;87;0
WireConnection;90;2;68;0
WireConnection;30;0;7;3
WireConnection;30;1;90;0
WireConnection;3;0;7;0
WireConnection;3;1;7;1
WireConnection;3;2;7;2
WireConnection;0;2;3;0
WireConnection;0;9;30;0
ASEEND*/
//CHKSM=304FB655714A764625CDA3BD9693D236D4F756D2