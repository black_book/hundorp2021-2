// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "BB Toon Shader with Dissolve"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		_Albedo("Albedo", 2D) = "white" {}
		_Tint("Tint", Color) = (0.5471698,0.5471698,0.5471698,0)
		_YGradient("Y Gradient", Color) = (1,0,0,0)
		_Normal("Normal", 2D) = "bump" {}
		_Gradient("Gradient", 2D) = "white" {}
		_ShadowPower("ShadowPower", Range( 0 , 1)) = 0
		_RimPower("RimPower", Range( -1 , 1)) = 0.2705882
		_RimPower2("RimPower2", Range( -1 , 1)) = 0.07058824
		_RimOffset("RimOffset", Range( 1 , 20)) = 0.8
		_RimStep("RimStep", Range( 0 , 0.5)) = 0.3411765
		_UseCustomRim("UseCustomRim", Range( 0 , 1)) = 0
		_Rimcolor("Rim color", Color) = (1,0.9294118,0.517,1)
		_ObjectHeight("Object Height", Float) = 1
		_CenterOffset("Center Offset", Float) = 0
		[Toggle]_PreviewHeightGradient("Preview Height Gradient", Float) = 0
		_Timeline("_Timeline", Range( 0 , 1)) = 0
		[KeywordEnum(BottomToTop,TopToBottom)] _DissolveDirection("Dissolve Direction", Float) = 0
		[KeywordEnum(X,Y,Z)] _DissolveAxis("Dissolve Axis", Float) = 0
		_NoiseTexture("Noise Texture", 2D) = "white" {}
		_DissolveWidth("Dissolve Width", Float) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "Geometry+0" }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityStandardUtils.cginc"
		#include "UnityCG.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#pragma shader_feature_local _DISSOLVEDIRECTION_BOTTOMTOTOP _DISSOLVEDIRECTION_TOPTOBOTTOM
		#pragma shader_feature_local _DISSOLVEAXIS_X _DISSOLVEAXIS_Y _DISSOLVEAXIS_Z
		struct Input
		{
			float4 screenPos;
			float3 worldPos;
			float2 uv_texcoord;
			float3 worldNormal;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform sampler2D _NoiseTexture;
		uniform float4 _NoiseTexture_ST;
		float4 _NoiseTexture_TexelSize;
		uniform float _DissolveWidth;
		uniform float _ObjectHeight;
		uniform float _CenterOffset;
		uniform float _Timeline;
		uniform float _PreviewHeightGradient;
		uniform float _RimStep;
		uniform sampler2D _Normal;
		uniform float4 _Normal_ST;
		uniform float _RimOffset;
		uniform float _RimPower;
		uniform float4 _Rimcolor;
		uniform float _UseCustomRim;
		uniform float _RimPower2;
		uniform float4 _YGradient;
		uniform sampler2D _Gradient;
		uniform float _ShadowPower;
		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform float4 _Tint;
		uniform float _Cutoff = 0.5;


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float4 break323 = ( ase_screenPosNorm * _ScreenParams * _NoiseTexture_TexelSize );
			float2 appendResult337 = (float2(break323.x , break323.y));
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			#if defined(_DISSOLVEAXIS_X)
				float staticSwitch349 = ase_vertex3Pos.x;
			#elif defined(_DISSOLVEAXIS_Y)
				float staticSwitch349 = ase_vertex3Pos.y;
			#elif defined(_DISSOLVEAXIS_Z)
				float staticSwitch349 = ase_vertex3Pos.z;
			#else
				float staticSwitch349 = ase_vertex3Pos.x;
			#endif
			float Height280 = _ObjectHeight;
			float HeightGradient270 = ( ( staticSwitch349 / Height280 ) - _CenterOffset );
			#if defined(_DISSOLVEDIRECTION_BOTTOMTOTOP)
				float staticSwitch284 = ( ( 0.5 - HeightGradient270 ) + _Timeline );
			#elif defined(_DISSOLVEDIRECTION_TOPTOBOTTOM)
				float staticSwitch284 = ( ( HeightGradient270 - 0.5 ) + _Timeline );
			#else
				float staticSwitch284 = ( ( 0.5 - HeightGradient270 ) + _Timeline );
			#endif
			float temp_output_339_0 = ( ( tex2D( _NoiseTexture, ( _NoiseTexture_ST.xy * ( appendResult337 + _NoiseTexture_ST.zw ) ) ).r + ( _DissolveWidth * staticSwitch284 ) ) * ( _DissolveWidth * staticSwitch284 ) );
			float simplePerlin2D208 = snoise( i.uv_texcoord*21.02 );
			simplePerlin2D208 = simplePerlin2D208*0.5 + 0.5;
			float2 uv_Normal = i.uv_texcoord * _Normal_ST.xy + _Normal_ST.zw;
			float3 ase_worldNormal = i.worldNormal;
			float3 Normals2 = BlendNormals( UnpackNormal( tex2D( _Normal, uv_Normal ) ) , ase_worldNormal );
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldViewDir = Unity_SafeNormalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float dotResult11 = dot( Normals2 , ase_worldViewDir );
			float NormalViewDir12 = dotResult11;
			float smoothstepResult120 = smoothstep( ( 0.5 - _RimStep ) , ( 0.5 + _RimStep ) , pow( ( 1.0 - NormalViewDir12 ) , _RimOffset ));
			float clampResult190 = clamp( ( ( simplePerlin2D208 * smoothstepResult120 ) + smoothstepResult120 ) , 0.0 , 1.0 );
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = Unity_SafeNormalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float dotResult5 = dot( Normals2 , ase_worldlightDir );
			float NormalLightDir6 = dotResult5;
			float dotResult186 = dot( clampResult190 , NormalLightDir6 );
			float clampResult234 = clamp( dotResult186 , 0.0 , 1.0 );
			float rimLight184 = clampResult234;
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			float4 lerpResult117 = lerp( float4( ase_lightColor.rgb , 0.0 ) , _Rimcolor , _UseCustomRim);
			float dotResult189 = dot( clampResult190 , -NormalLightDir6 );
			float clampResult235 = clamp( dotResult189 , 0.0 , 1.0 );
			float rimDark199 = clampResult235;
			float4 rimcolored60 = ( ( rimLight184 * _RimPower * lerpResult117 ) + ( lerpResult117 * _RimPower2 * rimDark199 ) );
			float4 clampResult236 = clamp( rimcolored60 , float4( 0,0,0,0 ) , float4( 1,1,1,1 ) );
			float2 temp_cast_1 = ((NormalLightDir6*0.5 + 0.5)).xx;
			float4 clampResult182 = clamp( ( tex2D( _Gradient, temp_cast_1 ) + ( 1.0 - _ShadowPower ) ) , float4( 0,0,0,0 ) , float4( 1,1,1,1 ) );
			float4 Shadow27 = clampResult182;
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float4 Albedo21 = ( tex2D( _Albedo, uv_Albedo ) * _Tint );
			float clampResult226 = clamp( ( ( 1.0 - _YGradient.a ) + ase_screenPosNorm.y ) , 0.0 , 1.0 );
			float4 lerpResult232 = lerp( _YGradient , ( Shadow27 * ase_lightColor * Albedo21 ) , clampResult226);
			float4 temp_cast_2 = (( ( tex2D( _NoiseTexture, ( _NoiseTexture_ST.xy * ( appendResult337 + _NoiseTexture_ST.zw ) ) ).r + ( _DissolveWidth * staticSwitch284 ) ) * ( _DissolveWidth * staticSwitch284 ) )).xxxx;
			c.rgb = (( _PreviewHeightGradient )?( temp_cast_2 ):( ( clampResult236 + ( ( 1.0 - -rimcolored60 ) * lerpResult232 ) ) )).rgb;
			c.a = 1;
			clip( temp_output_339_0 - _Cutoff );
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				float4 screenPos : TEXCOORD3;
				float3 worldNormal : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.worldNormal = worldNormal;
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				o.screenPos = ComputeScreenPos( o.pos );
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = IN.worldNormal;
				surfIN.screenPos = IN.screenPos;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				UnityGI gi;
				UNITY_INITIALIZE_OUTPUT( UnityGI, gi );
				o.Alpha = LightingStandardCustomLighting( o, worldViewDir, gi ).a;
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18910
327.2;728.8;1378.4;703;1338.778;1360.803;2.26882;True;False
Node;AmplifyShaderEditor.CommentaryNode;29;-3677.883,-465.4883;Inherit;False;877.6201;460;Comment;4;2;109;1;252;Normal;1,1,1,1;0;0
Node;AmplifyShaderEditor.WorldNormalVector;252;-3511.315,-187.789;Inherit;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SamplerNode;1;-3613.883,-409.4879;Inherit;True;Property;_Normal;Normal;4;0;Create;True;0;0;0;False;0;False;-1;None;78a2320b37e04634ba21c965c47ba816;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.BlendNormalsNode;109;-3241.015,-314.5445;Inherit;False;0;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;2;-3008.258,-316.5373;Inherit;True;Normals;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;13;-3629.254,582.164;Inherit;False;801.348;348.861;Comment;4;10;12;11;9;Normal.ViewDir;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;10;-3610.521,642.3304;Inherit;False;2;Normals;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;9;-3603.742,739.6169;Inherit;False;World;True;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.DotProductOpNode;11;-3275.321,677.2737;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;183;-2628.215,-339.4781;Inherit;False;2101.488;694.7988;Comment;21;184;199;186;189;216;190;214;213;212;120;208;125;209;124;112;111;123;44;107;234;235;Rim;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;12;-3038.902,664.5674;Inherit;True;NormalViewDir;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;107;-2591.734,-280.4789;Inherit;True;12;NormalViewDir;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;123;-2478.651,151.8531;Inherit;False;Property;_RimStep;RimStep;10;0;Create;True;0;0;0;False;0;False;0.3411765;0.16;0;0.5;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;44;-2485.254,70.05681;Inherit;False;Property;_RimOffset;RimOffset;9;0;Create;True;0;0;0;False;0;False;0.8;2;1;20;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;111;-2355.666,-153.5439;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;14;-3565.182,126.4187;Inherit;False;708.422;360.4709;Comment;4;6;5;7;4;Normal.LightDir;1,1,1,1;0;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;209;-2149.566,-275.4202;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PowerNode;112;-2148.302,-141.2821;Inherit;True;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;4;-3503.247,293.9077;Inherit;False;True;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleSubtractOpNode;124;-2078.652,84.7531;Inherit;False;2;0;FLOAT;0.5;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;125;-2069.452,177.9533;Inherit;False;2;2;0;FLOAT;0.5;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;7;-3451.34,195.3105;Inherit;False;2;Normals;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SmoothstepOpNode;120;-1862.74,-106.9173;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;5;-3219.232,235.4653;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;208;-1880.4,-277.5797;Inherit;False;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;21.02;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;6;-3072.372,221.9518;Inherit;True;NormalLightDir;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;212;-1677.047,-250.0178;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;214;-1604.757,135.5718;Inherit;False;6;NormalLightDir;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;271;-1086.205,-805.6685;Inherit;False;1280.161;312.5052;Comment;8;270;266;265;263;280;253;262;349;Height Gradient;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleAddOpNode;213;-1539.047,-125.0179;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NegateNode;216;-1376.234,181.2159;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;190;-1393.66,-101.5824;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;262;-1069.342,-599.0807;Inherit;False;Property;_ObjectHeight;Object Height;13;0;Create;True;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;253;-1064.208,-757.6686;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DotProductOpNode;189;-1194.66,85.41862;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;349;-874.7803,-739.3939;Inherit;False;Property;_DissolveAxis;Dissolve Axis;18;0;Create;True;0;0;0;False;0;False;0;0;0;True;;KeywordEnum;3;X;Y;Z;Create;True;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;186;-1198.403,-158.6708;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;28;-2633.366,-994.7539;Inherit;False;1517.68;527.7238;Comment;9;181;182;27;146;24;16;180;17;15;Shadow;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;280;-895.0164,-598.4814;Inherit;False;Height;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;265;-651.3555,-602.02;Inherit;False;Property;_CenterOffset;Center Offset;14;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;263;-649.7637,-706.0885;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;106;-1803.742,445.6338;Inherit;False;1288.164;557.8646;Rim2;12;60;203;115;200;50;117;202;201;185;118;56;119;Rim2;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;17;-2554.61,-717.5873;Inherit;False;Constant;_Float0;Float 0;1;0;Create;True;0;0;0;False;0;False;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;340;-265.6562,-1591.843;Inherit;False;1363.784;602.1669;Comment;10;314;332;292;335;323;337;334;312;313;315;Screen Space Blue Noise;1,1,1,1;0;0
Node;AmplifyShaderEditor.ClampOpNode;234;-939.9139,-144.7589;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;235;-930.9139,103.241;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;15;-2610.367,-907.7849;Inherit;True;6;NormalLightDir;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;146;-2088.359,-582.2803;Inherit;False;Property;_ShadowPower;ShadowPower;6;0;Create;True;0;0;0;False;0;False;0;0.39;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;313;-203.0139,-1541.843;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleSubtractOpNode;266;-454.5119,-710.4767;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;199;-741.5942,109.5307;Inherit;False;rimDark;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LightColorNode;119;-1656.302,569.2727;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.RegisterLocalVarNode;184;-735.6287,-158.5015;Inherit;False;rimLight;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;56;-1710.078,693.5717;Inherit;False;Property;_Rimcolor;Rim color;12;0;Create;True;0;0;0;False;0;False;1,0.9294118,0.517,1;0.1542898,0.4150943,0.242777,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenParams;312;-189.9136,-1369.143;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexelSizeNode;314;-215.6562,-1198.676;Inherit;False;292;1;0;SAMPLER2D;;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;118;-1777.935,876.5088;Inherit;False;Property;_UseCustomRim;UseCustomRim;11;0;Create;True;0;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;16;-2324.491,-856.7303;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;291;245.5794,-863.1927;Inherit;False;839.0235;355.0854;Comment;6;272;285;288;276;284;290;Animate Gradient;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;270;-257.0216,-709.9048;Inherit;False;HeightGradient;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;201;-1296.751,907.6635;Inherit;False;199;rimDark;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;24;-2024.978,-847.9986;Inherit;True;Property;_Gradient;Gradient;5;0;Create;True;0;0;0;False;0;False;-1;None;ffe65d51e4b2a8d4ea1da48dbf62df9d;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;50;-1398.957,575.4575;Inherit;False;Property;_RimPower;RimPower;7;0;Create;True;0;0;0;False;0;False;0.2705882;0.25;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;315;63.35463,-1368.57;Inherit;False;3;3;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.CommentaryNode;22;-3659.32,-990.3914;Inherit;False;835.1768;440.5575;Comment;4;21;20;19;18;Albedo;1,1,1,1;0;0
Node;AmplifyShaderEditor.OneMinusNode;181;-1819.033,-577.8215;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;117;-1399.238,671.0995;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;202;-1400.751,823.6635;Inherit;False;Property;_RimPower2;RimPower2;8;0;Create;True;0;0;0;False;0;False;0.07058824;0.6;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;185;-1292.911,492.5904;Inherit;False;184;rimLight;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;115;-1087.283,555.8085;Inherit;True;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;41;-348.6304,-330.6257;Inherit;False;1786.154;725.226;Comment;16;63;251;236;250;232;247;226;33;30;225;32;25;42;222;228;233;Lighting;1,1,1,1;0;0
Node;AmplifyShaderEditor.ColorNode;19;-3530.175,-733.9653;Inherit;False;Property;_Tint;Tint;2;0;Create;True;0;0;0;False;0;False;0.5471698,0.5471698,0.5471698,0;0.3926219,0.4811321,0.4811321,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;272;298.5299,-717.0731;Inherit;False;Property;_Timeline;_Timeline;16;0;Create;True;0;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;18;-3600.234,-932.2792;Inherit;True;Property;_Albedo;Albedo;1;0;Create;True;0;0;0;False;0;False;-1;None;42473e980b5ddaa459ccc601a4fe56b8;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;200;-1087.75,772.6635;Inherit;True;3;3;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;285;296.8745,-812.8356;Inherit;False;2;0;FLOAT;0.5;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;323;205.0964,-1368.567;Inherit;False;FLOAT4;1;0;FLOAT4;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SimpleSubtractOpNode;290;295.5793,-641.9072;Inherit;False;2;0;FLOAT;0.5;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;180;-1624.033,-769.8218;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;276;578.5058,-813.1927;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureTransformNode;332;256.811,-1219.406;Inherit;False;292;False;1;0;SAMPLER2D;;False;2;FLOAT2;0;FLOAT2;1
Node;AmplifyShaderEditor.SimpleAddOpNode;288;581.4747,-650.591;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;337;331.2941,-1368.297;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;-3276.638,-792.3802;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;203;-836.7495,651.6635;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;233;-298.4296,48.60289;Inherit;False;Property;_YGradient;Y Gradient;3;0;Create;True;0;0;0;False;0;False;1,0,0,0;1,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ClampOpNode;182;-1472.033,-743.8218;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;1,1,1,1;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;335;485.294,-1275.297;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;342;961.8179,-952.3011;Inherit;False;Property;_DissolveWidth;Dissolve Width;20;0;Create;True;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;228;-58.07011,126.9136;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;222;-98.72211,208.5014;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;60;-704.5544,644.4225;Inherit;False;rimcolored;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StaticSwitch;284;775.2029,-750.9221;Inherit;False;Property;_DissolveDirection;Dissolve Direction;17;0;Create;True;0;0;0;False;0;False;0;0;0;True;;KeywordEnum;2;BottomToTop;TopToBottom;Create;True;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;27;-1319.451,-768.225;Inherit;True;Shadow;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21;-3125.352,-779.1824;Inherit;False;Albedo;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LightColorNode;32;-252.0497,-175.5767;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.GetLocalVarNode;25;-264.382,-46.75584;Inherit;False;21;Albedo;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;225;116.8595,129.3407;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;334;634.2314,-1214.699;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;42;263.1294,-264.0969;Inherit;True;60;rimcolored;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;30;-270.4879,-267.7259;Inherit;False;27;Shadow;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;341;1196.055,-770.1163;Inherit;False;2;2;0;FLOAT;10;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NegateNode;247;543.5084,-52.59348;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ClampOpNode;226;269.8596,133.3407;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;292;776.5279,-1241.07;Inherit;True;Property;_NoiseTexture;Noise Texture;19;0;Create;True;0;0;0;False;0;False;-1;27bc543b9fe328a4288f1b271db1dd0f;27bc543b9fe328a4288f1b271db1dd0f;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;33;263.5589,-63.51948;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RelayNode;269;1392.039,-742.9229;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;232;441.0406,42.75907;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;250;689.0513,-53.31607;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;338;1568.826,-817.2546;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;236;899.1097,-269.9155;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;1,1,1,1;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;251;918.3585,-26.41825;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;305;1728.252,-764.7703;Inherit;False;2;2;0;FLOAT;4;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;63;1206.081,-266.7521;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RelayNode;339;1917.242,-765.6533;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;267;2109.329,-681.0089;Inherit;False;Property;_PreviewHeightGradient;Preview Height Gradient;15;0;Create;True;0;0;0;False;0;False;0;True;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;2443.57,-972.1763;Float;False;True;-1;2;ASEMaterialInspector;0;0;CustomLighting;BB Toon Shader with Dissolve;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;TransparentCutout;;Geometry;All;16;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;109;0;1;0
WireConnection;109;1;252;0
WireConnection;2;0;109;0
WireConnection;11;0;10;0
WireConnection;11;1;9;0
WireConnection;12;0;11;0
WireConnection;111;0;107;0
WireConnection;112;0;111;0
WireConnection;112;1;44;0
WireConnection;124;1;123;0
WireConnection;125;1;123;0
WireConnection;120;0;112;0
WireConnection;120;1;124;0
WireConnection;120;2;125;0
WireConnection;5;0;7;0
WireConnection;5;1;4;0
WireConnection;208;0;209;0
WireConnection;6;0;5;0
WireConnection;212;0;208;0
WireConnection;212;1;120;0
WireConnection;213;0;212;0
WireConnection;213;1;120;0
WireConnection;216;0;214;0
WireConnection;190;0;213;0
WireConnection;189;0;190;0
WireConnection;189;1;216;0
WireConnection;349;1;253;1
WireConnection;349;0;253;2
WireConnection;349;2;253;3
WireConnection;186;0;190;0
WireConnection;186;1;214;0
WireConnection;280;0;262;0
WireConnection;263;0;349;0
WireConnection;263;1;280;0
WireConnection;234;0;186;0
WireConnection;235;0;189;0
WireConnection;266;0;263;0
WireConnection;266;1;265;0
WireConnection;199;0;235;0
WireConnection;184;0;234;0
WireConnection;16;0;15;0
WireConnection;16;1;17;0
WireConnection;16;2;17;0
WireConnection;270;0;266;0
WireConnection;24;1;16;0
WireConnection;315;0;313;0
WireConnection;315;1;312;0
WireConnection;315;2;314;0
WireConnection;181;0;146;0
WireConnection;117;0;119;1
WireConnection;117;1;56;0
WireConnection;117;2;118;0
WireConnection;115;0;185;0
WireConnection;115;1;50;0
WireConnection;115;2;117;0
WireConnection;200;0;117;0
WireConnection;200;1;202;0
WireConnection;200;2;201;0
WireConnection;285;1;270;0
WireConnection;323;0;315;0
WireConnection;290;0;270;0
WireConnection;180;0;24;0
WireConnection;180;1;181;0
WireConnection;276;0;285;0
WireConnection;276;1;272;0
WireConnection;288;0;290;0
WireConnection;288;1;272;0
WireConnection;337;0;323;0
WireConnection;337;1;323;1
WireConnection;20;0;18;0
WireConnection;20;1;19;0
WireConnection;203;0;115;0
WireConnection;203;1;200;0
WireConnection;182;0;180;0
WireConnection;335;0;337;0
WireConnection;335;1;332;1
WireConnection;228;0;233;4
WireConnection;60;0;203;0
WireConnection;284;1;276;0
WireConnection;284;0;288;0
WireConnection;27;0;182;0
WireConnection;21;0;20;0
WireConnection;225;0;228;0
WireConnection;225;1;222;2
WireConnection;334;0;332;0
WireConnection;334;1;335;0
WireConnection;341;0;342;0
WireConnection;341;1;284;0
WireConnection;247;0;42;0
WireConnection;226;0;225;0
WireConnection;292;1;334;0
WireConnection;33;0;30;0
WireConnection;33;1;32;0
WireConnection;33;2;25;0
WireConnection;269;0;341;0
WireConnection;232;0;233;0
WireConnection;232;1;33;0
WireConnection;232;2;226;0
WireConnection;250;0;247;0
WireConnection;338;0;292;1
WireConnection;338;1;269;0
WireConnection;236;0;42;0
WireConnection;251;0;250;0
WireConnection;251;1;232;0
WireConnection;305;0;338;0
WireConnection;305;1;269;0
WireConnection;63;0;236;0
WireConnection;63;1;251;0
WireConnection;339;0;305;0
WireConnection;267;0;63;0
WireConnection;267;1;339;0
WireConnection;0;10;339;0
WireConnection;0;13;267;0
ASEEND*/
//CHKSM=3C99616AA423CE3CBF3B4CE3FAA78D84B8364162