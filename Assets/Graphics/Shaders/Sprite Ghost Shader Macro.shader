// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Dissolve Macro"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		_Timeline("_Timeline", Range( 0 , 1)) = 0.4718255
		_Texture0("Texture 0", 2D) = "white" {}
		_TopTexture1("Top Texture 1", 2D) = "white" {}
		_UpperLimit("UpperLimit", Range( 0 , 2)) = 0.55
		_LowerLimit("LowerLimit", Range( -1 , 1)) = 0.3176471
		_Float0("Float 0", Range( 0 , 10)) = 1
		_Float5("Float 5", Range( 0 , 2)) = 2
		_Float6("Float 6", Range( 0 , 2)) = 1
		_TilingMultiplierDissolve("Tiling Multiplier Dissolve", Float) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "AlphaTest+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		ZTest Always
		Blend SrcAlpha OneMinusSrcAlpha
		
		CGPROGRAM
		#pragma target 5.0
		#pragma surface surf Standard keepalpha noshadow 
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
			float3 worldNormal;
			INTERNAL_DATA
		};

		uniform sampler2D _Texture0;
		uniform float4 _Texture0_ST;
		sampler2D _TopTexture1;
		uniform float _Float6;
		uniform float _Float5;
		uniform float _Timeline;
		uniform float _TilingMultiplierDissolve;
		uniform float _Float0;
		uniform float _LowerLimit;
		uniform float _UpperLimit;
		uniform float _Cutoff = 0.5;


		inline float4 TriplanarSampling200( sampler2D topTexMap, float3 worldPos, float3 worldNormal, float falloff, float2 tiling, float3 normalScale, float3 index )
		{
			float3 projNormal = ( pow( abs( worldNormal ), falloff ) );
			projNormal /= ( projNormal.x + projNormal.y + projNormal.z ) + 0.00001;
			float3 nsign = sign( worldNormal );
			half4 xNorm; half4 yNorm; half4 zNorm;
			xNorm = tex2D( topTexMap, tiling * worldPos.zy * float2(  nsign.x, 1.0 ) );
			yNorm = tex2D( topTexMap, tiling * worldPos.xz * float2(  nsign.y, 1.0 ) );
			zNorm = tex2D( topTexMap, tiling * worldPos.xy * float2( -nsign.z, 1.0 ) );
			return xNorm * projNormal.x + yNorm * projNormal.y + zNorm * projNormal.z;
		}


		void surf( Input i , inout SurfaceOutputStandard o )
		{
			o.Normal = float3(0,0,1);
			float4 color52 = IsGammaSpace() ? float4(0,0.3080587,1.898039,1) : float4(0,0.07729272,4.095164,1);
			float2 uv_Texture0 = i.uv_texcoord * _Texture0_ST.xy + _Texture0_ST.zw;
			float4 tex2DNode69 = tex2D( _Texture0, uv_Texture0 );
			float4 lerpResult145 = lerp( color52 , tex2DNode69 , tex2DNode69.a);
			float4 RimlightDiffuse220 = lerpResult145;
			o.Albedo = RimlightDiffuse220.rgb;
			float4 RimlightEmission221 = ( color52 * ( 1.0 - tex2DNode69.a ) );
			o.Emission = RimlightEmission221.rgb;
			o.Alpha = 1;
			float4 appendResult196 = (float4(_Float6 , ( 1.0 - ( _Float5 * ( ( 1.0 - _Timeline ) * i.uv_texcoord.y ) ) ) , 0.0 , 0.0));
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float temp_output_184_0 = ( _Float0 * ( ( i.uv_texcoord.y + 0.0 ) * _Timeline ) );
			float2 appendResult186 = (float2(temp_output_184_0 , temp_output_184_0));
			float2 break243 = ( i.uv_texcoord + appendResult186 );
			float3 appendResult244 = (float3(break243.x , break243.y , break243.x));
			float4 triplanar200 = TriplanarSampling200( _TopTexture1, appendResult244, ase_worldNormal, 1.0, ( appendResult196 * _TilingMultiplierDissolve ).xy, 1.0, 0 );
			float4 clampResult235 = clamp( ( ( i.uv_texcoord.y * triplanar200 ) + (_LowerLimit + (_Timeline - 0.0) * (_UpperLimit - _LowerLimit) / (1.0 - 0.0)) ) , float4( 0,0,0,0 ) , float4( 1,1,1,1 ) );
			float4 DissolveAlpha225 = clampResult235;
			clip( DissolveAlpha225.x - _Cutoff );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18910
336;877.6;2121.2;809.8;6655.866;1158.86;1.877862;True;False
Node;AmplifyShaderEditor.CommentaryNode;175;-6582.623,-956.0848;Inherit;False;1586.689;443.4172;Distort;8;246;181;186;184;182;180;189;178;;1,1,1,1;0;0
Node;AmplifyShaderEditor.TexCoordVertexDataNode;178;-6558.246,-909.4824;Inherit;False;0;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;180;-6322.675,-744.3544;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;176;-6547.884,-229.3413;Inherit;False;Property;_Timeline;_Timeline;1;0;Create;True;0;0;0;False;0;False;0.4718255;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;183;-5884.656,-219.5602;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;181;-6075.713,-836.601;Inherit;False;Property;_Float0;Float 0;6;0;Create;True;0;0;0;False;0;False;1;1;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;182;-6004.895,-744.9227;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TexCoordVertexDataNode;185;-5892.828,-140.4292;Inherit;False;0;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;188;-5675.598,-218.5418;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;187;-6001.093,-310.89;Inherit;False;Property;_Float5;Float 5;7;0;Create;True;0;0;0;False;0;False;2;1.047645;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;184;-5737.188,-761.5789;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;190;-5510.785,-309.4212;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;186;-5428.616,-714.3783;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RelayNode;246;-5413.17,-910.0242;Inherit;False;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;193;-6010.784,-412.2001;Inherit;False;Property;_Float6;Float 6;8;0;Create;True;0;0;0;False;0;False;1;0.8124995;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;189;-5198.94,-737.1729;Inherit;True;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.OneMinusNode;192;-5266.667,-314.0442;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;243;-4916.23,-563.065;Inherit;False;FLOAT2;1;0;FLOAT2;0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.DynamicAppendNode;196;-5020.481,-423.9496;Inherit;True;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;240;-5041.467,-198.7227;Inherit;False;Property;_TilingMultiplierDissolve;Tiling Multiplier Dissolve;9;0;Create;True;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;244;-4754.283,-562.2166;Inherit;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.TexturePropertyNode;117;-4678.422,-961.8973;Inherit;True;Property;_Texture0;Texture 0;2;0;Create;True;0;0;0;False;0;False;None;None;False;white;Auto;Texture2D;-1;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;239;-4754.467,-423.7228;Inherit;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SamplerNode;69;-4428.382,-961.772;Inherit;True;Property;_TextureSample2;Texture Sample 2;1;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;198;-4504.372,138.3269;Inherit;False;Property;_UpperLimit;UpperLimit;4;0;Create;True;0;0;0;False;0;False;0.55;0.55;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.TexCoordVertexDataNode;195;-4378.683,-648.8453;Inherit;False;0;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TriplanarNode;200;-4571.67,-515.8832;Inherit;True;Spherical;World;False;Top Texture 1;_TopTexture1;white;3;None;Mid Texture 1;_MidTexture1;white;-1;None;Bot Texture 1;_BotTexture1;white;-1;None;Triplanar Sampler;Tangent;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;201;-4504.271,59.47656;Inherit;False;Property;_LowerLimit;LowerLimit;5;0;Create;True;0;0;0;False;0;False;0.3176471;0.3176471;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RelayNode;245;-5893.293,9.516193;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;203;-4133.184,-2.16779;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;136;-4085.845,-889.6191;Inherit;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;202;-4144.77,-536.2502;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.ColorNode;52;-4335.25,-1138.037;Inherit;False;Constant;_Color0;Color 0;3;1;[HDR];Create;True;0;0;0;False;0;False;0,0.3080587,1.898039,1;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;150;-3911.926,-1063.133;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;205;-3883.391,-536.3917;Inherit;True;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.ClampOpNode;235;-3637.98,-533.5143;Inherit;True;3;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT4;1,1,1,1;False;1;FLOAT4;0
Node;AmplifyShaderEditor.LerpOp;145;-3911.152,-979.4111;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;151;-3698.467,-1131.054;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;220;-3350.22,-718.0663;Inherit;False;RimlightDiffuse;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;221;-3367.888,-1041.491;Inherit;True;RimlightEmission;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;225;-3348.273,-537.3586;Inherit;False;DissolveAlpha;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;230;-3115.743,-546.6386;Inherit;False;225;DissolveAlpha;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;231;-3126.865,-706.2539;Inherit;False;221;RimlightEmission;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;228;-3138.073,-998.7775;Inherit;True;220;RimlightDiffuse;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;-2835.628,-849.7066;Float;False;True;-1;7;ASEMaterialInspector;0;0;Standard;Dissolve Macro;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;7;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;False;0;True;Transparent;;AlphaTest;All;16;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;180;0;178;2
WireConnection;183;0;176;0
WireConnection;182;0;180;0
WireConnection;182;1;176;0
WireConnection;188;0;183;0
WireConnection;188;1;185;2
WireConnection;184;0;181;0
WireConnection;184;1;182;0
WireConnection;190;0;187;0
WireConnection;190;1;188;0
WireConnection;186;0;184;0
WireConnection;186;1;184;0
WireConnection;246;0;178;0
WireConnection;189;0;246;0
WireConnection;189;1;186;0
WireConnection;192;0;190;0
WireConnection;243;0;189;0
WireConnection;196;0;193;0
WireConnection;196;1;192;0
WireConnection;244;0;243;0
WireConnection;244;1;243;1
WireConnection;244;2;243;0
WireConnection;239;0;196;0
WireConnection;239;1;240;0
WireConnection;69;0;117;0
WireConnection;200;9;244;0
WireConnection;200;3;239;0
WireConnection;245;0;176;0
WireConnection;203;0;245;0
WireConnection;203;3;201;0
WireConnection;203;4;198;0
WireConnection;136;0;69;0
WireConnection;202;0;195;2
WireConnection;202;1;200;0
WireConnection;150;0;136;3
WireConnection;205;0;202;0
WireConnection;205;1;203;0
WireConnection;235;0;205;0
WireConnection;145;0;52;0
WireConnection;145;1;69;0
WireConnection;145;2;136;3
WireConnection;151;0;52;0
WireConnection;151;1;150;0
WireConnection;220;0;145;0
WireConnection;221;0;151;0
WireConnection;225;0;235;0
WireConnection;0;0;228;0
WireConnection;0;2;231;0
WireConnection;0;10;230;0
ASEEND*/
//CHKSM=05889DAF1B97EBD806CB60842B6CC4E651DEF8ED