// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "BB Toon Shader"
{
	Properties
	{
		_Albedo("Albedo", 2D) = "white" {}
		_Tint("Tint", Color) = (0.5471698,0.5471698,0.5471698,0)
		_YGradient("Y Gradient", Color) = (1,0,0,0)
		_Normal("Normal", 2D) = "bump" {}
		_Gradient("Gradient", 2D) = "white" {}
		_ShadowPower("ShadowPower", Range( 0 , 1)) = 0
		_RimPower("RimPower", Range( -1 , 1)) = 0.2705882
		_RimPower2("RimPower2", Range( -1 , 1)) = 0.07058824
		_RimOffset("RimOffset", Range( 1 , 20)) = 0.8
		_RimStep("RimStep", Range( 0 , 0.5)) = 0.3411765
		_UseCustomRim("UseCustomRim", Range( 0 , 1)) = 0
		_Rimcolor("Rim color", Color) = (1,0.9294118,0.517,1)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityStandardUtils.cginc"
		#include "UnityCG.cginc"
		#include "UnityShaderVariables.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		struct Input
		{
			float2 uv_texcoord;
			float3 worldNormal;
			float3 worldPos;
			float4 screenPos;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform float4 _YGradient;
		uniform sampler2D _Gradient;
		uniform sampler2D _Normal;
		uniform float4 _Normal_ST;
		uniform float _ShadowPower;
		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform float4 _Tint;
		uniform float _RimStep;
		uniform float _RimOffset;
		uniform float _RimPower;
		uniform float4 _Rimcolor;
		uniform float _UseCustomRim;
		uniform float _RimPower2;


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			float2 uv_Normal = i.uv_texcoord * _Normal_ST.xy + _Normal_ST.zw;
			float3 ase_worldNormal = i.worldNormal;
			float3 Normals2 = BlendNormals( UnpackNormal( tex2D( _Normal, uv_Normal ) ) , ase_worldNormal );
			float3 ase_worldPos = i.worldPos;
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = Unity_SafeNormalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float dotResult5 = dot( Normals2 , ase_worldlightDir );
			float NormalLightDir6 = dotResult5;
			float2 temp_cast_0 = ((NormalLightDir6*0.5 + 0.5)).xx;
			float4 clampResult182 = clamp( ( tex2D( _Gradient, temp_cast_0 ) + ( 1.0 - _ShadowPower ) ) , float4( 0,0,0,0 ) , float4( 1,1,1,1 ) );
			float4 Shadow27 = clampResult182;
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float4 Albedo21 = ( tex2D( _Albedo, uv_Albedo ) * _Tint );
			float4 lighting35 = ( Shadow27 * ase_lightColor * Albedo21 );
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float clampResult226 = clamp( ( ase_screenPosNorm.y + ( 1.0 - _YGradient.a ) ) , 0.0 , 1.0 );
			float4 lerpResult232 = lerp( _YGradient , lighting35 , clampResult226);
			float simplePerlin2D208 = snoise( i.uv_texcoord*21.02 );
			simplePerlin2D208 = simplePerlin2D208*0.5 + 0.5;
			float3 ase_worldViewDir = Unity_SafeNormalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float dotResult11 = dot( Normals2 , ase_worldViewDir );
			float NormalViewDir12 = dotResult11;
			float smoothstepResult120 = smoothstep( ( 0.5 - _RimStep ) , ( 0.5 + _RimStep ) , pow( ( 1.0 - NormalViewDir12 ) , _RimOffset ));
			float clampResult190 = clamp( ( ( simplePerlin2D208 * smoothstepResult120 ) + smoothstepResult120 ) , 0.0 , 1.0 );
			float dotResult186 = dot( clampResult190 , NormalLightDir6 );
			float clampResult234 = clamp( dotResult186 , 0.0 , 1.0 );
			float rimLight184 = clampResult234;
			float4 lerpResult117 = lerp( float4( ase_lightColor.rgb , 0.0 ) , _Rimcolor , _UseCustomRim);
			float dotResult189 = dot( clampResult190 , -NormalLightDir6 );
			float clampResult235 = clamp( dotResult189 , 0.0 , 1.0 );
			float rimDark199 = clampResult235;
			float4 rimcolored60 = ( ( rimLight184 * _RimPower * lerpResult117 ) + ( rimDark199 * _RimPower2 * lerpResult117 ) );
			float4 clampResult236 = clamp( rimcolored60 , float4( 0,0,0,0 ) , float4( 1,1,1,1 ) );
			c.rgb = ( ( lerpResult232 * ( 1.0 - -rimcolored60 ) ) + clampResult236 ).rgb;
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				float4 screenPos : TEXCOORD3;
				float3 worldNormal : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.worldNormal = worldNormal;
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				o.screenPos = ComputeScreenPos( o.pos );
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = IN.worldNormal;
				surfIN.screenPos = IN.screenPos;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18909
2816;-2;1577;948;4205.835;639.8177;1.177158;True;False
Node;AmplifyShaderEditor.CommentaryNode;29;-3677.883,-465.4883;Inherit;False;877.6201;460;Comment;5;2;109;108;1;252;Normal;1,1,1,1;0;0
Node;AmplifyShaderEditor.WorldNormalVector;252;-3511.315,-187.789;Inherit;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SamplerNode;1;-3613.883,-409.4879;Inherit;True;Property;_Normal;Normal;3;0;Create;True;0;0;0;False;0;False;-1;None;cb7c9c8faa59dc5479f4737499ad4b16;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.BlendNormalsNode;109;-3241.015,-314.5445;Inherit;False;0;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;13;-3629.254,582.164;Inherit;False;801.348;348.861;Comment;4;10;12;11;9;Normal.ViewDir;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;2;-3008.258,-316.5373;Inherit;True;Normals;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;10;-3610.521,642.3304;Inherit;False;2;Normals;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;9;-3603.742,739.6169;Inherit;False;World;True;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.DotProductOpNode;11;-3275.321,677.2737;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;183;-2616.835,248.4957;Inherit;False;2101.488;694.7988;Comment;21;184;199;186;189;216;190;214;213;212;120;208;125;209;124;112;111;123;44;107;234;235;Rim;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;12;-3038.902,664.5674;Inherit;True;NormalViewDir;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;107;-2580.354,307.4948;Inherit;True;12;NormalViewDir;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;111;-2344.286,434.4299;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;14;-3565.182,126.4187;Inherit;False;708.422;360.4709;Comment;4;6;5;7;4;Normal.LightDir;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;123;-2467.271,739.827;Inherit;False;Property;_RimStep;RimStep;15;0;Create;True;0;0;0;False;0;False;0.3411765;0.13;0;0.5;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;44;-2473.874,658.0307;Inherit;False;Property;_RimOffset;RimOffset;14;0;Create;True;0;0;0;False;0;False;0.8;1.94;1;20;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;7;-3451.34,195.3105;Inherit;False;2;Normals;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;124;-2067.272,672.727;Inherit;False;2;0;FLOAT;0.5;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;209;-2138.186,312.5536;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;125;-2058.072,765.9272;Inherit;False;2;2;0;FLOAT;0.5;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;112;-2136.922,446.6917;Inherit;True;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;4;-3503.247,293.9077;Inherit;False;True;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.NoiseGeneratorNode;208;-1869.02,310.3941;Inherit;False;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;21.02;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;120;-1851.36,481.0565;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;5;-3219.232,235.4653;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;6;-3072.372,221.9518;Inherit;True;NormalLightDir;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;212;-1665.667,337.956;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;214;-1593.377,723.5458;Inherit;False;6;NormalLightDir;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;213;-1527.667,462.9559;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;28;-2455.077,-956.8202;Inherit;False;1517.68;527.7238;Comment;9;181;182;27;146;24;16;180;17;15;Shadow;1,1,1,1;0;0
Node;AmplifyShaderEditor.ClampOpNode;190;-1382.28,486.3914;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.NegateNode;216;-1364.854,769.1898;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;17;-2376.32,-679.6536;Inherit;False;Constant;_Float0;Float 0;1;0;Create;True;0;0;0;False;0;False;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;15;-2432.078,-869.8512;Inherit;True;6;NormalLightDir;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;189;-1183.28,673.3925;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;186;-1187.023,429.303;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;146;-1910.069,-544.3466;Inherit;False;Property;_ShadowPower;ShadowPower;5;0;Create;True;0;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;16;-2146.202,-818.7966;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;235;-919.5339,691.2149;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;106;-2323.994,1053.308;Inherit;False;1435.889;594.7958;Rim2;12;60;203;115;200;201;202;117;185;50;119;118;56;Rim2;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;22;-3659.32,-990.3914;Inherit;False;835.1768;440.5575;Comment;4;21;20;19;18;Albedo;1,1,1,1;0;0
Node;AmplifyShaderEditor.OneMinusNode;181;-1640.744,-539.8878;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;234;-928.5339,443.2149;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;24;-1846.689,-810.0649;Inherit;True;Property;_Gradient;Gradient;4;0;Create;True;0;0;0;False;0;False;-1;None;e020e08db2822a34b96c879f41320f96;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;184;-724.2486,429.4723;Inherit;False;rimLight;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LightColorNode;119;-2166.482,1099.727;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.RangedFloatNode;118;-2288.115,1406.963;Inherit;False;Property;_UseCustomRim;UseCustomRim;16;0;Create;True;0;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;56;-2220.258,1224.026;Inherit;False;Property;_Rimcolor;Rim color;17;0;Create;True;0;0;0;False;0;False;1,0.9294118,0.517,1;1,0.8943856,0.2783019,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;199;-730.2142,697.5046;Inherit;False;rimDark;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;19;-3530.175,-733.9653;Inherit;False;Property;_Tint;Tint;1;0;Create;True;0;0;0;False;0;False;0.5471698,0.5471698,0.5471698,0;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;180;-1445.744,-731.8881;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;18;-3600.234,-932.2792;Inherit;True;Property;_Albedo;Albedo;0;0;Create;True;0;0;0;False;0;False;-1;None;9bdeeca5bad47de439b83476d9c92558;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;202;-1834.995,1456.695;Inherit;False;Property;_RimPower2;RimPower2;13;0;Create;True;0;0;0;False;0;False;0.07058824;-0.42;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;50;-1803.201,1175.489;Inherit;False;Property;_RimPower;RimPower;12;0;Create;True;0;0;0;False;0;False;0.2705882;0.609;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;185;-1701.156,1101.622;Inherit;False;184;rimLight;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;201;-1727.995,1334.695;Inherit;False;199;rimDark;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;182;-1293.744,-705.8881;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;1,1,1,1;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;-3276.638,-792.3802;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;117;-1968.482,1223.131;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21;-3125.352,-779.1824;Inherit;False;Albedo;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;27;-1141.162,-730.2913;Inherit;True;Shadow;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;115;-1513.528,1166.84;Inherit;True;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;200;-1513.995,1383.695;Inherit;True;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;41;-1728.975,-363.1051;Inherit;False;789.34;337.3216;Comment;5;30;32;25;33;35;Lighting;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleAddOpNode;203;-1275.995,1322.695;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LightColorNode;32;-1687.394,-218.0559;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.ColorNode;233;-395.8282,-111.6668;Inherit;False;Property;_YGradient;Y Gradient;2;0;Create;True;0;0;0;False;0;False;1,0,0,0;0,0.2075472,0.02306081,0.3254902;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;30;-1587.832,-309.2051;Inherit;False;27;Shadow;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;25;-1540.726,-130.2351;Inherit;False;21;Albedo;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;228;-258.8384,346.9878;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;60;-1136.8,1327.454;Inherit;False;rimcolored;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;33;-1337.785,-265.9987;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;222;-274.3282,154.0012;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;225;-76.90875,249.4149;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;42;136.1539,439.4033;Inherit;True;60;rimcolored;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;35;-1159.636,-275.1232;Inherit;True;lighting;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;151;49.51335,91.23573;Inherit;True;35;lighting;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.NegateNode;247;368.5377,400.8294;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ClampOpNode;226;96.0912,297.4149;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;250;522.1425,383.9617;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;232;308.2722,114.8333;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;251;770.1425,176.9617;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ClampOpNode;236;755.2346,433.5847;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;1,1,1,1;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;128;-2595.413,1804.902;Inherit;False;2189.725;935.6213;Specular;20;88;89;87;78;79;140;68;81;84;73;70;132;138;69;85;136;76;137;83;145;Specular;1,1,1,1;0;0
Node;AmplifyShaderEditor.NormalVertexDataNode;108;-3742.761,-185.5561;Inherit;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;81;-637.363,2017.07;Inherit;False;Spec;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;69;-2548.26,2043.975;Inherit;False;2;Normals;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;140;-1607.905,2145.262;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;62;787.0793,-206.4838;Inherit;True;27;Shadow;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;145;-1562.752,2456.418;Inherit;False;Property;_SpecularColor;SpecularColor;11;0;Create;True;0;0;0;False;0;False;0,0,0,0;1,0.9302049,0.4292453,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;78;-1334.596,2171.958;Inherit;False;Property;_SpecInitensity;Spec Initensity;9;0;Create;True;0;0;0;False;0;False;1;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldNormalVector;68;-2359.168,2122.376;Inherit;False;True;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.LightColorNode;87;-1509.608,2333.878;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.OneMinusNode;132;-1587.238,1992.428;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;89;-1249.182,2366.771;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SmoothstepOpNode;73;-1244.874,1942.602;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;84;-852.5864,2020.635;Inherit;True;4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;137;-2095.238,2401.428;Inherit;False;Property;_RoughMultiplier;RoughMultiplier;8;0;Create;True;0;0;0;False;0;False;0.7058824;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;224;521.0913,-281.5851;Inherit;False;1;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;83;-2104.899,2214.308;Inherit;True;Property;_Roughness;Roughness;7;0;Create;True;0;0;0;False;0;False;-1;None;ce13f4f46ea356642a738750476f417d;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;76;-2094.113,2138.404;Inherit;False;Property;_Gloss;Gloss;6;0;Create;True;0;0;0;False;0;False;0.33;0.02;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldSpaceLightPos;85;-2549.767,1856.416;Inherit;False;0;3;FLOAT4;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.GetLocalVarNode;82;764.7107,680.4188;Inherit;True;81;Spec;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;63;1068.072,247.9613;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;138;-1430.238,2068.428;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;136;-1766.238,2281.428;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LightAttenuation;79;-1267.513,2263.219;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;88;-1625.84,2631.782;Inherit;False;Property;_UseCustomSpec;UseCustomSpec;10;0;Create;True;0;0;0;False;0;False;0;0.845;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;70;-2138.616,1902.339;Inherit;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1350.331,109.3256;Float;False;True;-1;2;ASEMaterialInspector;0;0;CustomLighting;BB Toon Shader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;16;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;109;0;1;0
WireConnection;109;1;252;0
WireConnection;2;0;109;0
WireConnection;11;0;10;0
WireConnection;11;1;9;0
WireConnection;12;0;11;0
WireConnection;111;0;107;0
WireConnection;124;1;123;0
WireConnection;125;1;123;0
WireConnection;112;0;111;0
WireConnection;112;1;44;0
WireConnection;208;0;209;0
WireConnection;120;0;112;0
WireConnection;120;1;124;0
WireConnection;120;2;125;0
WireConnection;5;0;7;0
WireConnection;5;1;4;0
WireConnection;6;0;5;0
WireConnection;212;0;208;0
WireConnection;212;1;120;0
WireConnection;213;0;212;0
WireConnection;213;1;120;0
WireConnection;190;0;213;0
WireConnection;216;0;214;0
WireConnection;189;0;190;0
WireConnection;189;1;216;0
WireConnection;186;0;190;0
WireConnection;186;1;214;0
WireConnection;16;0;15;0
WireConnection;16;1;17;0
WireConnection;16;2;17;0
WireConnection;235;0;189;0
WireConnection;181;0;146;0
WireConnection;234;0;186;0
WireConnection;24;1;16;0
WireConnection;184;0;234;0
WireConnection;199;0;235;0
WireConnection;180;0;24;0
WireConnection;180;1;181;0
WireConnection;182;0;180;0
WireConnection;20;0;18;0
WireConnection;20;1;19;0
WireConnection;117;0;119;1
WireConnection;117;1;56;0
WireConnection;117;2;118;0
WireConnection;21;0;20;0
WireConnection;27;0;182;0
WireConnection;115;0;185;0
WireConnection;115;1;50;0
WireConnection;115;2;117;0
WireConnection;200;0;201;0
WireConnection;200;1;202;0
WireConnection;200;2;117;0
WireConnection;203;0;115;0
WireConnection;203;1;200;0
WireConnection;228;0;233;4
WireConnection;60;0;203;0
WireConnection;33;0;30;0
WireConnection;33;1;32;0
WireConnection;33;2;25;0
WireConnection;225;0;222;2
WireConnection;225;1;228;0
WireConnection;35;0;33;0
WireConnection;247;0;42;0
WireConnection;226;0;225;0
WireConnection;250;0;247;0
WireConnection;232;0;233;0
WireConnection;232;1;151;0
WireConnection;232;2;226;0
WireConnection;251;0;232;0
WireConnection;251;1;250;0
WireConnection;236;0;42;0
WireConnection;81;0;84;0
WireConnection;140;0;76;0
WireConnection;140;1;136;0
WireConnection;68;0;69;0
WireConnection;132;0;76;0
WireConnection;89;0;87;0
WireConnection;89;1;145;0
WireConnection;89;2;88;0
WireConnection;73;0;70;0
WireConnection;73;1;132;0
WireConnection;73;2;138;0
WireConnection;84;0;73;0
WireConnection;84;1;78;0
WireConnection;84;2;79;0
WireConnection;84;3;89;0
WireConnection;63;0;251;0
WireConnection;63;1;236;0
WireConnection;138;0;140;0
WireConnection;136;0;83;1
WireConnection;136;1;137;0
WireConnection;70;0;85;1
WireConnection;70;1;69;0
WireConnection;0;13;63;0
ASEEND*/
//CHKSM=4289693AA30FC20558F1D891DDA1A88A940BA89F