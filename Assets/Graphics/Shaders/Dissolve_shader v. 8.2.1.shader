// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Dissolve_shader v.8.2.1"
{
	Properties
	{
		_TextureSample2("Texture Sample 2", 2D) = "white" {}
		_UpperLimit("UpperLimit", Range( 0 , 2)) = 0.55
		_LowerLimit("LowerLimit", Range( -1 , 1)) = 0.3176471
		_Timeline("_Timeline", Range( 0 , 1)) = 0.4718255
		_NoiseScale("Noise Scale", Range( 0 , 100)) = 36.67862
		_TimeScaleSmall("TimeScaleSmall", Float) = 0.5
		[HDR]_Beamcolor("Beam color", Color) = (0,0,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" }
		Cull Off
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 5.0
		#pragma surface surf Standard alpha:fade keepalpha noshadow 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float4 _Beamcolor;
		uniform sampler2D _TextureSample2;
		uniform float4 _TextureSample2_ST;
		uniform float _Timeline;
		uniform float _LowerLimit;
		uniform float _UpperLimit;
		uniform float _TimeScaleSmall;
		uniform float _NoiseScale;


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_TextureSample2 = i.uv_texcoord * _TextureSample2_ST.xy + _TextureSample2_ST.zw;
			float4 tex2DNode57 = tex2D( _TextureSample2, uv_TextureSample2 );
			o.Albedo = ( _Beamcolor * tex2DNode57 ).rgb;
			float temp_output_77_0 = ( 1.0 - i.uv_texcoord.y );
			float _Timeline70 = _Timeline;
			float temp_output_15_0 = ( temp_output_77_0 + (_LowerLimit + (_Timeline70 - 0.0) * (_UpperLimit - _LowerLimit) / (1.0 - 0.0)) );
			float mulTime139 = _Time.y * _TimeScaleSmall;
			float2 appendResult134 = (float2(0.0 , ( 1.0 - mulTime139 )));
			float2 uv_TexCoord135 = i.uv_texcoord + appendResult134;
			float simplePerlin2D137 = snoise( uv_TexCoord135*_NoiseScale );
			simplePerlin2D137 = simplePerlin2D137*0.5 + 0.5;
			o.Alpha = ( tex2DNode57.a * temp_output_15_0 * simplePerlin2D137 );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18800
-1536;5.6;1536;797.4;3661.51;38.37366;1.732251;True;False
Node;AmplifyShaderEditor.RangedFloatNode;138;-3264.067,1108.915;Inherit;False;Property;_TimeScaleSmall;TimeScaleSmall;11;0;Create;True;0;0;0;False;0;False;0.5;0.08;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;67;-5633.288,755.3777;Inherit;False;Property;_Timeline;_Timeline;5;0;Create;True;0;0;0;False;0;False;0.4718255;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;139;-3043.746,1071.109;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;141;-2845.093,998.1026;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;70;-5202.887,749.0814;Inherit;False;_Timeline;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;19;-2492.619,540.6483;Inherit;False;Property;_LowerLimit;LowerLimit;4;0;Create;True;0;0;0;False;0;False;0.3176471;-0.906;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;134;-2676.84,884.3688;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;21;-2449.719,621.4987;Inherit;False;Property;_UpperLimit;UpperLimit;3;0;Create;True;0;0;0;False;0;False;0.55;0.991;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.TexCoordVertexDataNode;6;-2636.262,-16.96805;Inherit;False;0;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;72;-2558.09,443.886;Inherit;False;70;_Timeline;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;136;-2397.784,954.5382;Inherit;False;Property;_NoiseScale;Noise Scale;8;0;Create;True;0;0;0;False;0;False;36.67862;100;0;100;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;135;-2428.902,817.9609;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCRemapNode;22;-2121.531,479.0039;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;77;-2276.106,-1.021896;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;137;-2046.965,891.3468;Inherit;True;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;57;-1144.4,-199.8074;Inherit;True;Property;_TextureSample2;Texture Sample 2;1;0;Create;True;0;0;0;False;0;False;-1;None;4947b45c47e3f4a4490bec10b2201dc3;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;142;-739.3014,-341.134;Inherit;False;Property;_Beamcolor;Beam color;12;1;[HDR];Create;True;0;0;0;False;0;False;0,0,0,0;4.036711,4.793594,24.09412,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;54;-4661.951,-366.078;Inherit;False;1698.068;585.0474;Distort;9;41;51;52;94;97;99;101;100;133;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleAddOpNode;15;-1806.388,275.1152;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;118;-3809.902,615.1758;Inherit;True;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleAddOpNode;94;-4196.503,-149.3472;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;10;-2146.694,239.6996;Inherit;True;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;56;-3330.603,276.7967;Inherit;False;55;DistortEffect;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.OneMinusNode;123;-5349.246,863.8099;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;119;-4285.047,544.8809;Inherit;False;Property;_Float2;Float 2;9;0;Create;True;0;0;0;False;0;False;1;0;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;101;-3688.764,-167.0707;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;99;-3987.29,120.032;Inherit;False;Property;_Float0;Float 0;6;0;Create;True;0;0;0;False;0;False;1;10;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.TexCoordVertexDataNode;120;-5588.223,930.4012;Inherit;False;0;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;68;-418.9966,224.225;Inherit;True;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TriplanarNode;115;-3120.024,341.877;Inherit;True;Spherical;World;False;Top Texture 0;_TopTexture0;white;0;None;Mid Texture 0;_MidTexture0;white;-1;None;Bot Texture 0;_BotTexture0;white;-1;None;Triplanar Sampler;Tangent;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;117;-4932.083,765.9667;Inherit;False;Property;_Float1;Float 1;7;0;Create;True;0;0;0;False;0;False;2;0.09;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;97;-4273.671,108.8972;Inherit;False;70;_Timeline;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;51;-3461.194,-193.124;Inherit;True;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleAddOpNode;52;-3198.882,-316.078;Inherit;True;2;2;0;FLOAT2;0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SamplerNode;13;-3213.566,623.6017;Inherit;True;Property;_TextureSample1;Texture Sample 1;2;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;124;-4835.238,916.9283;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;55;-2889.284,-293.5216;Inherit;False;DistortEffect;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.OneMinusNode;130;-4056.087,725.0812;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;81;-1626.11,15.31059;Inherit;False;Constant;_Color0;Color 0;9;1;[HDR];Create;True;0;0;0;False;0;False;1,1,1,1;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexCoordVertexDataNode;41;-4611.951,-264.8929;Inherit;False;0;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;133;-4382.64,-163.2455;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;82;-535.6407,-43.19249;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;125;-4527.865,772.9307;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;143;-283.5999,-199.6626;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;86;-1149.877,89.79071;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;128;-4610.164,1101.421;Inherit;False;Property;_Float3;Float 3;10;0;Create;True;0;0;0;False;0;False;0.1117433;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;100;-3928.846,-127.4143;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;127;-4269.425,795.2016;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;95,-34;Float;False;True;-1;7;ASEMaterialInspector;0;0;Standard;Dissolve_shader v.8.2.1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.5;True;False;0;False;Transparent;;Transparent;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;139;0;138;0
WireConnection;141;0;139;0
WireConnection;70;0;67;0
WireConnection;134;1;141;0
WireConnection;135;1;134;0
WireConnection;22;0;72;0
WireConnection;22;3;19;0
WireConnection;22;4;21;0
WireConnection;77;0;6;2
WireConnection;137;0;135;0
WireConnection;137;1;136;0
WireConnection;15;0;77;0
WireConnection;15;1;22;0
WireConnection;118;0;119;0
WireConnection;118;1;130;0
WireConnection;94;0;133;0
WireConnection;10;0;115;0
WireConnection;10;1;77;0
WireConnection;123;0;70;0
WireConnection;101;0;100;0
WireConnection;101;1;99;0
WireConnection;68;0;57;4
WireConnection;68;1;15;0
WireConnection;68;2;137;0
WireConnection;115;9;56;0
WireConnection;115;3;118;0
WireConnection;51;0;101;0
WireConnection;51;1;101;0
WireConnection;52;0;41;0
WireConnection;52;1;51;0
WireConnection;124;0;123;0
WireConnection;124;1;120;2
WireConnection;55;0;52;0
WireConnection;130;0;125;0
WireConnection;133;0;41;2
WireConnection;82;0;86;0
WireConnection;82;1;57;4
WireConnection;125;0;117;0
WireConnection;125;1;124;0
WireConnection;143;0;142;0
WireConnection;143;1;57;0
WireConnection;86;0;81;0
WireConnection;86;1;15;0
WireConnection;100;0;94;0
WireConnection;100;1;97;0
WireConnection;127;0;125;0
WireConnection;127;1;128;0
WireConnection;0;0;143;0
WireConnection;0;9;68;0
ASEEND*/
//CHKSM=D81E30FCB0949CC7FBC18FA736A1D808B9B14CDF