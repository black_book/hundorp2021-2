using UnityEngine;
 using UnityEngine.UI;



/*
 * This script is created by Richard Bielawski. If there are any questions, please send me an email to richardabielawski@gmail.com
 * 
 * The purpose of this script is to create a parallax effect with HIDDENs AR technology. It uses ARFoundation's AR Camera transform, to move different layers of sprites accordingly to their relative distance. Attach this script to every sprite/layer the prefab/scene is made of.
 * 
 * The variables works like this: The Distance is the length the layer is away from the last layer (from viewpoint). So last layer has 0 in Distance, and closest could have 200 Distance. Set up the scene with Distance first, and after that you can give it a multiplier. Both variables can increase the parallax effect, but if you have set the Distance
 * correctly, but the effect is too strong. For that I have implemented the Effectmultiplier, so if you reduce that, the total effect changes. Try to have the same EffectMultiplier value throughout the prefab, but only the Distance is changing.
 */
 public class Parallax : MonoBehaviour
 {
    
    private Camera cam;
    private Transform cameraTransform; // Camera reference (of its transform)
    Vector3 previousCamPos;
    [Tooltip("The Distance is the length the layer is away from the last layer (from viewpoint). So the layer furthest away from the camera has 0 in Distance, and closest layer could have 200 Distance. Set up the scene with Distance first, and after that you can give it a multiplier. Both variables can increase the parallax effect, but if you have set the Distance " +
        "correctly, but the effect is too strong. ")]
    public float Distance;
    [Tooltip("For that I have implemented the Effectmultiplier, so if you reduce that, the total effect changes.Try to have the same EffectMultiplier value throughout the prefab, but only the Distance is changing.")]
    public float EffectMultiplier = 1f;
    


    /* For testing/development purposes only
     * Attach sliders in the correct slot in the insepctor. Use sliders with numbers from 1-10. This aids in in-game polishing. Remember to use variables TestEffectMultiplier and TestDistance when using sliders. I recommend using one slider at a time.
     * 
    public float TestDistance;
    public float TestEffectMultiplier;
    public Slider sliderDistance;
    public Slider sliderEffect;
    
    public void SliderEffectMultiplier(float normalized)
    {
        EffectMultiplier = TestEffectMultiplier * normalized;
    }
    public void SliderDistance(float normalized)
    {
       Distance = TestDistance * normalized;
    }
    */



    void Start()
    {
        // Often this script is attached inside prefabs, remember to give "AR camera" in Main scene the tag "MainCamera", this way the script finds the AR camera when the scene is loaded with the prefab
        cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>() as Camera;
        cameraTransform = cam.transform;
        previousCamPos = cameraTransform.position;
    }
    void Update()
    {
        if (Distance != 0f)
        {
            float parallaxX = (previousCamPos.x - cameraTransform.position.x) * Distance;
            float parallaxY = (previousCamPos.y - cameraTransform.position.y) * Distance;
            float parallaxZ = (previousCamPos.z - cameraTransform.position.z) * Distance;
            Vector3 backgroundTargetPosX = new Vector3(transform.position.x + parallaxX,
                                                      transform.position.y,
                                                      transform.position.z);
            transform.position = Vector3.Lerp(transform.position, backgroundTargetPosX, EffectMultiplier * Time.deltaTime);
            Vector3 backgroundTargetPosY = new Vector3(transform.position.x,
                                                       transform.position.y + parallaxY,
                                                       transform.position.z);
            transform.position = Vector3.Lerp(transform.position, backgroundTargetPosY, EffectMultiplier * Time.deltaTime);
            Vector3 backgroundTargetPosZ = new Vector3(transform.position.x,
                                                       transform.position.y,
                                                       transform.position.z + parallaxZ);
            transform.position = Vector3.Lerp(transform.position, backgroundTargetPosZ, EffectMultiplier * Time.deltaTime);
        }


        previousCamPos = cameraTransform.position;
    }
 }