// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "BlurBehind"
{
	Properties
	{
		_yOffset("yOffset", Range( 0 , 0.01)) = 0
		_xOffset("xOffset", Range( 0 , 0.01)) = 0
		_Tint("Tint", Color) = (1,1,1,1)
		[Toggle(SHADER_API_METAL)] _SHADER_API_METAL("SHADER_API_METAL", Float) = 0
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		GrabPass{ }
		CGPROGRAM
		#pragma target 3.0
		#if defined(UNITY_STEREO_INSTANCING_ENABLED) || defined(UNITY_STEREO_MULTIVIEW_ENABLED)
		#define ASE_DECLARE_SCREENSPACE_TEXTURE(tex) UNITY_DECLARE_SCREENSPACE_TEXTURE(tex);
		#else
		#define ASE_DECLARE_SCREENSPACE_TEXTURE(tex) UNITY_DECLARE_SCREENSPACE_TEXTURE(tex)
		#endif
		#pragma surface surf Unlit keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float4 screenPos;
		};

		ASE_DECLARE_SCREENSPACE_TEXTURE( _GrabTexture )
		uniform float _yOffset;
		uniform float _xOffset;
		uniform float4 _Tint;

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			#ifdef SHADER_API_METAL
				float staticSwitch95 = ( 1.0 - ase_screenPosNorm.y );
			#else
				float staticSwitch95 = ase_screenPosNorm.y;
			#endif
			float temp_output_33_0 = ( staticSwitch95 + _yOffset );
			float2 appendResult14 = (float2(ase_screenPosNorm.x , temp_output_33_0));
			float4 screenColor11 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_GrabTexture,appendResult14);
			float temp_output_34_0 = ( staticSwitch95 - _yOffset );
			float2 appendResult16 = (float2(ase_screenPosNorm.x , temp_output_34_0));
			float4 screenColor13 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_GrabTexture,appendResult16);
			float4 lerpResult23 = lerp( screenColor11 , screenColor13 , 0.5);
			float temp_output_32_0 = ( ase_screenPosNorm.x - _xOffset );
			float2 appendResult6 = (float2(temp_output_32_0 , temp_output_33_0));
			float4 screenColor2 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_GrabTexture,appendResult6);
			float2 appendResult8 = (float2(temp_output_32_0 , staticSwitch95));
			float4 screenColor7 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_GrabTexture,appendResult8);
			float4 lerpResult5 = lerp( screenColor2 , screenColor7 , 0.5);
			float4 lerpResult28 = lerp( lerpResult23 , lerpResult5 , 0.5);
			float2 appendResult10 = (float2(temp_output_32_0 , temp_output_34_0));
			float4 screenColor9 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_GrabTexture,appendResult10);
			float temp_output_31_0 = ( ase_screenPosNorm.x + _xOffset );
			float2 appendResult22 = (float2(temp_output_31_0 , temp_output_34_0));
			float4 screenColor19 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_GrabTexture,appendResult22);
			float4 lerpResult26 = lerp( screenColor9 , screenColor19 , 0.5);
			float2 appendResult20 = (float2(temp_output_31_0 , temp_output_33_0));
			float4 screenColor17 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_GrabTexture,appendResult20);
			float2 appendResult21 = (float2(temp_output_31_0 , staticSwitch95));
			float4 screenColor18 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_GrabTexture,appendResult21);
			float4 lerpResult24 = lerp( screenColor17 , screenColor18 , 0.5);
			float4 lerpResult29 = lerp( lerpResult26 , lerpResult24 , 0.5);
			float4 lerpResult30 = lerp( lerpResult28 , lerpResult29 , 0.5);
			#ifdef SHADER_API_METAL
				float staticSwitch96 = ( 1.0 - ase_screenPosNorm.y );
			#else
				float staticSwitch96 = ase_screenPosNorm.y;
			#endif
			float temp_output_74_0 = ( _yOffset * 2.0 );
			float temp_output_42_0 = ( staticSwitch96 + temp_output_74_0 );
			float2 appendResult51 = (float2(ase_screenPosNorm.x , temp_output_42_0));
			float4 screenColor59 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_GrabTexture,appendResult51);
			float temp_output_41_0 = ( staticSwitch96 - temp_output_74_0 );
			float2 appendResult47 = (float2(ase_screenPosNorm.x , temp_output_41_0));
			float4 screenColor53 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_GrabTexture,appendResult47);
			float4 lerpResult62 = lerp( screenColor59 , screenColor53 , 0.5);
			float temp_output_71_0 = ( _xOffset * 2.0 );
			float temp_output_44_0 = ( ase_screenPosNorm.x - temp_output_71_0 );
			float2 appendResult49 = (float2(temp_output_44_0 , temp_output_42_0));
			float4 screenColor56 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_GrabTexture,appendResult49);
			float2 appendResult48 = (float2(temp_output_44_0 , staticSwitch96));
			float4 screenColor57 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_GrabTexture,appendResult48);
			float4 lerpResult64 = lerp( screenColor56 , screenColor57 , 0.5);
			float4 lerpResult66 = lerp( lerpResult62 , lerpResult64 , 0.5);
			float2 appendResult52 = (float2(temp_output_44_0 , temp_output_41_0));
			float4 screenColor58 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_GrabTexture,appendResult52);
			float temp_output_43_0 = ( ase_screenPosNorm.x + temp_output_71_0 );
			float2 appendResult45 = (float2(temp_output_43_0 , temp_output_41_0));
			float4 screenColor60 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_GrabTexture,appendResult45);
			float4 lerpResult65 = lerp( screenColor58 , screenColor60 , 0.5);
			float2 appendResult46 = (float2(temp_output_43_0 , temp_output_42_0));
			float4 screenColor54 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_GrabTexture,appendResult46);
			float2 appendResult50 = (float2(temp_output_43_0 , staticSwitch96));
			float4 screenColor61 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_GrabTexture,appendResult50);
			float4 lerpResult63 = lerp( screenColor54 , screenColor61 , 0.5);
			float4 lerpResult67 = lerp( lerpResult65 , lerpResult63 , 0.5);
			float4 lerpResult68 = lerp( lerpResult66 , lerpResult67 , 0.5);
			float4 lerpResult69 = lerp( lerpResult30 , lerpResult68 , 0.5);
			o.Emission = ( lerpResult69 * _Tint ).rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18910
0;1080;2832;1039;2498.167;-105.5702;1;True;False
Node;AmplifyShaderEditor.CommentaryNode;97;-1497.843,-805.1946;Inherit;False;483;223;Then flip the V axis of the screen UV;2;95;81;Check if iOS ;1,1,1,1;0;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;3;-1631.2,-1124.8;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenPosInputsNode;40;-1653.266,141.2549;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;98;-1491.937,531.8976;Inherit;False;483;223;Then flip the V axis of the screen UV;2;96;91;Check if iOS ;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;73;-1338.714,328.4483;Inherit;False;Constant;_Float3;Float 3;2;0;Create;True;0;0;0;False;0;False;2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;39;-1620.708,-122.2975;Inherit;False;Property;_yOffset;yOffset;0;0;Create;True;0;0;0;False;0;False;0;0.00261;0;0.01;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;91;-1447.148,624.9444;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;38;-1618.107,-231.4973;Inherit;False;Property;_xOffset;xOffset;1;0;Create;True;0;0;0;False;0;False;0;0.00379;0;0.01;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;81;-1460.798,-698.3141;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;95;-1258.793,-721.2079;Inherit;False;Property;_SHADER_API_METAL;SHADER_API_METAL;3;0;Create;True;0;0;0;False;0;False;0;0;0;True;SHADER_API_METAL;Toggle;2;Key0;Key1;Fetch;True;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;71;-1119.892,294.3223;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;74;-1107.546,398.851;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;96;-1249.375,600.4106;Inherit;False;Property;_SHADER_API_METAL;SHADER_API_METAL;3;0;Create;True;0;0;0;False;0;False;0;0;0;True;SHADER_API_METAL;Toggle;2;Key0;Key1;Fetch;True;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;42;-888.2391,423.5123;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;33;-938.9235,-793.812;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;43;-154.3741,168.2068;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;41;-888.6395,628.8121;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;32;-969.846,-987.197;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;31;-227.9456,-1071.297;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;34;-933.746,-605.8974;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;44;-878.3857,234.2021;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;49;-635.2934,302.6096;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;52;-633.7934,618.1097;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;48;-648.7934,443.1096;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;51;-314.6266,290.7763;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;21;-58.23331,-778.4332;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;22;-42.23331,-594.4332;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;47;-323.3155,611.7095;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;20;-44.73331,-944.9332;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;10;-710.9,-597.5999;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;6;-713.4,-948.0999;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;14;-386.7333,-943.9332;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;46;27.37335,289.7763;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;8;-714.9,-769.5999;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;45;29.87334,640.2763;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;16;-393.2333,-639.4332;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;50;18.87334,454.2764;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ScreenColorNode;17;83.2667,-947.9332;Inherit;False;Global;_GrabScreen10;Grab Screen 10;0;0;Create;True;0;0;0;False;0;False;Object;-1;False;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenColorNode;2;-587.9999,-954.9999;Inherit;False;Global;_GrabScreen0;Grab Screen 0;0;0;Create;True;0;0;0;False;0;False;Object;-1;False;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenColorNode;58;-510.7933,634.1097;Inherit;False;Global;_GrabScreen15;Grab Screen 15;0;0;Create;True;0;0;0;False;0;False;Object;-1;False;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenColorNode;59;-186.6267,287.7763;Inherit;False;Global;_GrabScreen11;Grab Screen 11;0;0;Create;True;0;0;0;False;0;False;Object;-1;False;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;27;-611.0451,-94.09745;Inherit;False;Constant;_Float0;Float 0;0;0;Create;True;0;0;0;False;0;False;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenColorNode;56;-515.8934,279.7096;Inherit;False;Global;_GrabScreen5;Grab Screen 5;0;0;Create;True;0;0;0;False;0;False;Object;-1;False;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenColorNode;13;-244.2333,-593.4332;Inherit;False;Global;_GrabScreen6;Grab Screen 6;0;0;Create;True;0;0;0;False;0;False;Object;-1;False;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenColorNode;53;-184.1267,638.2763;Inherit;False;Global;_GrabScreen9;Grab Screen 9;0;0;Create;True;0;0;0;False;0;False;Object;-1;False;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;55;-538.9386,1140.613;Inherit;False;Constant;_Float1;Float 1;0;0;Create;True;0;0;0;False;0;False;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenColorNode;19;85.7667,-597.4332;Inherit;False;Global;_GrabScreen7;Grab Screen 7;0;0;Create;True;0;0;0;False;0;False;Object;-1;False;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenColorNode;7;-583.9,-778.5999;Inherit;False;Global;_GrabScreen1;Grab Screen 1;0;0;Create;True;0;0;0;False;0;False;Object;-1;False;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenColorNode;11;-258.7333,-946.9332;Inherit;False;Global;_GrabScreen4;Grab Screen 4;0;0;Create;True;0;0;0;False;0;False;Object;-1;False;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenColorNode;61;156.8734,459.2763;Inherit;False;Global;_GrabScreen13;Grab Screen 13;0;0;Create;True;0;0;0;False;0;False;Object;-1;False;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenColorNode;60;157.8734,637.2763;Inherit;False;Global;_GrabScreen12;Grab Screen 12;0;0;Create;True;0;0;0;False;0;False;Object;-1;False;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenColorNode;9;-582.9,-600.5999;Inherit;False;Global;_GrabScreen2;Grab Screen 2;0;0;Create;True;0;0;0;False;0;False;Object;-1;False;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenColorNode;54;155.3734,286.7763;Inherit;False;Global;_GrabScreen3;Grab Screen 3;0;0;Create;True;0;0;0;False;0;False;Object;-1;False;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenColorNode;18;84.7667,-775.4332;Inherit;False;Global;_GrabScreen8;Grab Screen 8;0;0;Create;True;0;0;0;False;0;False;Object;-1;False;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenColorNode;57;-511.7933,456.1096;Inherit;False;Global;_GrabScreen14;Grab Screen 14;0;0;Create;True;0;0;0;False;0;False;Object;-1;False;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;26;-157.3453,-116.1977;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0.5;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;64;-308.0933,971.8097;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0.5;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;23;-152.1456,-383.9976;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0.5;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;63;109.761,963.8123;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0.5;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;5;-380.2,-262.9002;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0.5;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;65;-90.71086,1094.433;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0.5;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;24;37.65431,-270.8975;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0.5;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;62;-80.03896,850.7119;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0.5;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;67;139.6561,1259.869;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0.5;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;29;46.75495,16.40257;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0.5;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;66;-177.5383,1261.512;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0.5;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;28;-249.645,26.80247;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0.5;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;68;316.4614,1054.813;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0.5;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;70;427.8228,552.8612;Inherit;False;Constant;_Float2;Float 2;2;0;Create;True;0;0;0;False;0;False;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;30;244.3548,-179.8973;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0.5;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;76;696.08,651.6959;Inherit;False;Property;_Tint;Tint;2;0;Create;True;0;0;0;False;0;False;1,1,1,1;0.5471698,0.5471698,0.5471698,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;69;696.2227,513.2612;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;77;1075.36,565.6088;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;4;1277.516,487.3014;Float;False;True;-1;2;ASEMaterialInspector;0;0;Unlit;BlurBehind;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;16;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;91;0;40;2
WireConnection;81;0;3;2
WireConnection;95;1;3;2
WireConnection;95;0;81;0
WireConnection;71;0;38;0
WireConnection;71;1;73;0
WireConnection;74;0;39;0
WireConnection;74;1;73;0
WireConnection;96;1;40;2
WireConnection;96;0;91;0
WireConnection;42;0;96;0
WireConnection;42;1;74;0
WireConnection;33;0;95;0
WireConnection;33;1;39;0
WireConnection;43;0;40;1
WireConnection;43;1;71;0
WireConnection;41;0;96;0
WireConnection;41;1;74;0
WireConnection;32;0;3;1
WireConnection;32;1;38;0
WireConnection;31;0;3;1
WireConnection;31;1;38;0
WireConnection;34;0;95;0
WireConnection;34;1;39;0
WireConnection;44;0;40;1
WireConnection;44;1;71;0
WireConnection;49;0;44;0
WireConnection;49;1;42;0
WireConnection;52;0;44;0
WireConnection;52;1;41;0
WireConnection;48;0;44;0
WireConnection;48;1;96;0
WireConnection;51;0;40;1
WireConnection;51;1;42;0
WireConnection;21;0;31;0
WireConnection;21;1;95;0
WireConnection;22;0;31;0
WireConnection;22;1;34;0
WireConnection;47;0;40;1
WireConnection;47;1;41;0
WireConnection;20;0;31;0
WireConnection;20;1;33;0
WireConnection;10;0;32;0
WireConnection;10;1;34;0
WireConnection;6;0;32;0
WireConnection;6;1;33;0
WireConnection;14;0;3;1
WireConnection;14;1;33;0
WireConnection;46;0;43;0
WireConnection;46;1;42;0
WireConnection;8;0;32;0
WireConnection;8;1;95;0
WireConnection;45;0;43;0
WireConnection;45;1;41;0
WireConnection;16;0;3;1
WireConnection;16;1;34;0
WireConnection;50;0;43;0
WireConnection;50;1;96;0
WireConnection;17;0;20;0
WireConnection;2;0;6;0
WireConnection;58;0;52;0
WireConnection;59;0;51;0
WireConnection;56;0;49;0
WireConnection;13;0;16;0
WireConnection;53;0;47;0
WireConnection;19;0;22;0
WireConnection;7;0;8;0
WireConnection;11;0;14;0
WireConnection;61;0;50;0
WireConnection;60;0;45;0
WireConnection;9;0;10;0
WireConnection;54;0;46;0
WireConnection;18;0;21;0
WireConnection;57;0;48;0
WireConnection;26;0;9;0
WireConnection;26;1;19;0
WireConnection;26;2;27;0
WireConnection;64;0;56;0
WireConnection;64;1;57;0
WireConnection;64;2;55;0
WireConnection;23;0;11;0
WireConnection;23;1;13;0
WireConnection;23;2;27;0
WireConnection;63;0;54;0
WireConnection;63;1;61;0
WireConnection;63;2;55;0
WireConnection;5;0;2;0
WireConnection;5;1;7;0
WireConnection;5;2;27;0
WireConnection;65;0;58;0
WireConnection;65;1;60;0
WireConnection;65;2;55;0
WireConnection;24;0;17;0
WireConnection;24;1;18;0
WireConnection;24;2;27;0
WireConnection;62;0;59;0
WireConnection;62;1;53;0
WireConnection;62;2;55;0
WireConnection;67;0;65;0
WireConnection;67;1;63;0
WireConnection;67;2;55;0
WireConnection;29;0;26;0
WireConnection;29;1;24;0
WireConnection;29;2;27;0
WireConnection;66;0;62;0
WireConnection;66;1;64;0
WireConnection;66;2;55;0
WireConnection;28;0;23;0
WireConnection;28;1;5;0
WireConnection;28;2;27;0
WireConnection;68;0;66;0
WireConnection;68;1;67;0
WireConnection;68;2;55;0
WireConnection;30;0;28;0
WireConnection;30;1;29;0
WireConnection;30;2;27;0
WireConnection;69;0;30;0
WireConnection;69;1;68;0
WireConnection;69;2;70;0
WireConnection;77;0;69;0
WireConnection;77;1;76;0
WireConnection;4;2;77;0
ASEEND*/
//CHKSM=C1B57E78075F6D85A021D6FBB150EBF459C44029