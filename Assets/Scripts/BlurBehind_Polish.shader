// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "BlackBook/FX/Box Blur Behind"
{
	Properties
	{
		_offset("offset", Range( 0 , 0.01)) = 0
		_Tint("Tint", Color) = (1,1,1,0)
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "AlphaTest+0" "IsEmissive" = "true"  }
		Cull Back
		GrabPass{ "_ScreenTex" }
		CGPROGRAM
		#include "UnityCG.cginc"
		#pragma target 3.0
		#if defined(UNITY_STEREO_INSTANCING_ENABLED) || defined(UNITY_STEREO_MULTIVIEW_ENABLED)
		#define ASE_DECLARE_SCREENSPACE_TEXTURE(tex) UNITY_DECLARE_SCREENSPACE_TEXTURE(tex);
		#else
		#define ASE_DECLARE_SCREENSPACE_TEXTURE(tex) UNITY_DECLARE_SCREENSPACE_TEXTURE(tex)
		#endif
		#pragma surface surf Unlit keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float4 screenPos;
		};

		ASE_DECLARE_SCREENSPACE_TEXTURE( _ScreenTex )
		uniform float _offset;
		UNITY_DECLARE_DEPTH_TEXTURE( _CameraDepthTexture );
		uniform float4 _CameraDepthTexture_TexelSize;
		uniform float4 _Tint;

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float ScreenU12 = ase_screenPosNorm.x;
			float ScreenV15 = ase_screenPosNorm.y;
			#ifdef SHADER_API_METAL
				float staticSwitch83 = ( 1.0 - ScreenV15 );
			#else
				float staticSwitch83 = ScreenV15;
			#endif
			float2 appendResult27 = (float2(ScreenU12 , staticSwitch83));
			float4 screenColor32 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_ScreenTex,appendResult27);
			float clampDepth74 = SAMPLE_DEPTH_TEXTURE( _CameraDepthTexture, ase_screenPosNorm.xy );
			float offset11 = ( _offset * clampDepth74 );
			float temp_output_16_0 = ( ScreenU12 + offset11 );
			float2 appendResult29 = (float2(temp_output_16_0 , ScreenV15));
			float4 screenColor40 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_ScreenTex,appendResult29);
			float2 appendResult31 = (float2(( temp_output_16_0 + offset11 ) , ScreenV15));
			float4 screenColor48 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_ScreenTex,appendResult31);
			float temp_output_30_0 = ( ScreenU12 - offset11 );
			float2 appendResult47 = (float2(temp_output_30_0 , ScreenV15));
			float4 screenColor58 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_ScreenTex,appendResult47);
			float2 appendResult59 = (float2(( temp_output_30_0 - offset11 ) , ScreenV15));
			float4 screenColor64 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_ScreenTex,appendResult59);
			float temp_output_25_0 = ( ScreenV15 + offset11 );
			float2 appendResult33 = (float2(ScreenU12 , temp_output_25_0));
			float4 screenColor46 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_ScreenTex,appendResult33);
			float2 appendResult44 = (float2(ScreenU12 , ( temp_output_25_0 + offset11 )));
			float4 screenColor54 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_ScreenTex,appendResult44);
			float temp_output_42_0 = ( ScreenV15 - offset11 );
			float2 appendResult52 = (float2(ScreenU12 , temp_output_42_0));
			float4 screenColor63 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_ScreenTex,appendResult52);
			float2 appendResult61 = (float2(ScreenU12 , ( temp_output_42_0 - offset11 )));
			float4 screenColor65 = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_ScreenTex,appendResult61);
			o.Emission = ( ( ( ( ( ( ( ( ( ( screenColor32 + screenColor40 ) + screenColor48 ) + screenColor58 ) + screenColor64 ) + screenColor46 ) + screenColor54 ) + screenColor63 ) + screenColor65 ) * ( 1.0 / 9.0 ) ) * _Tint ).rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18910
1172;727;2667;1392;6582.797;1869.482;2.827564;True;False
Node;AmplifyShaderEditor.CommentaryNode;1;-4464.345,-1349.614;Inherit;False;1373;522;;10;15;27;32;12;11;78;74;9;10;85;Input;1,1,1,1;0;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;9;-4405.656,-1100.364;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenDepthNode;74;-4122.151,-1211.573;Inherit;False;1;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;10;-4143.631,-1294.02;Inherit;False;Property;_offset;offset;0;0;Create;True;0;0;0;False;0;False;0;0;0;0.01;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;78;-3802.297,-1231.424;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;85;-3911.783,-1037.533;Inherit;False;415;187;flip V if on iOS;2;83;84;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;2;-4039.604,-754.1011;Inherit;False;1565.4;1012.557;;24;66;64;62;59;58;57;51;49;48;47;40;36;34;31;30;29;24;22;19;18;17;16;14;13;Horizontal Blur;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;11;-3629.42,-1235.816;Inherit;False;offset;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;15;-4126.851,-999.1351;Inherit;False;ScreenV;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;12;-4125.094,-1115.327;Inherit;False;ScreenU;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;13;-4019.242,-635.8079;Inherit;False;12;ScreenU;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;14;-4018.445,-461.8344;Inherit;False;11;offset;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;84;-3880.124,-922.652;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;16;-3804.431,-643.4576;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;83;-3716.351,-993.1859;Inherit;False;Property;_SHADER_API_METAL;SHADER_API_METAL;2;0;Create;True;0;0;0;False;0;False;0;0;0;False;SHADER_API_METAL;Toggle;2;Key0;Key1;Fetch;True;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;17;-3615.772,-580.8746;Inherit;False;15;ScreenV;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;19;-3606.521,-477.7166;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;24;-3613.062,-356.017;Inherit;False;15;ScreenV;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;22;-4023.27,-5.818069;Inherit;False;11;offset;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;29;-3402.207,-640.0158;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;18;-4020.999,-189.9919;Inherit;False;12;ScreenU;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;27;-3445.472,-1105.881;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;31;-3410.559,-377.0466;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ScreenColorNode;40;-3216.58,-643.8857;Inherit;False;Global;_GrabScreen16;Grab Screen 16;0;0;Create;True;0;0;0;False;0;False;Instance;32;False;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenColorNode;32;-3281.344,-1037.766;Inherit;False;Global;_ScreenTex;ScreenTex;0;0;Create;True;0;0;0;False;0;False;Object;-1;True;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;34;-3599.202,-104.1847;Inherit;False;15;ScreenV;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;30;-3800.44,-182.9392;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;3;-3425.832,302.4212;Inherit;False;1538.716;924.8818;;24;68;65;67;61;60;63;53;52;54;55;73;42;44;46;72;39;33;35;71;41;25;70;21;20;Vertical Blur;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;20;-3402.456,375.6186;Inherit;False;15;ScreenV;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;47;-3397.255,-184.0624;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;21;-3399.456,626.6183;Inherit;False;11;offset;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;51;-2977.539,-667.2916;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;36;-3600.402,-22.07087;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;49;-3607.822,99.60303;Inherit;False;15;ScreenV;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenColorNode;48;-3224.14,-408.1984;Inherit;False;Global;_GrabScreen17;Grab Screen 17;0;0;Create;True;0;0;0;False;0;False;Instance;32;False;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenColorNode;58;-3224.892,-172.986;Inherit;False;Global;_GrabScreen0;Grab Screen 0;0;0;Create;True;0;0;0;False;0;False;Instance;32;False;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;59;-3404.09,76.74725;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;70;-3045.31,348.231;Inherit;False;12;ScreenU;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;25;-3202.376,379.6959;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;57;-2854.304,-412.4758;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;71;-3039.903,522.9601;Inherit;False;12;ScreenU;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;35;-3016.194,604.2291;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;39;-3403.194,867.1485;Inherit;False;15;ScreenV;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;41;-3399.463,1102.264;Inherit;False;11;offset;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;62;-2726.094,-175.4276;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.DynamicAppendNode;33;-2844.732,354.8859;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ScreenColorNode;64;-3230.997,73.12555;Inherit;False;Global;_GrabScreen20;Grab Screen 20;0;0;Create;True;0;0;0;False;0;False;Instance;32;False;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;72;-3046.903,813.8795;Inherit;False;12;ScreenU;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenColorNode;46;-2678.134,351.4611;Inherit;False;Global;_GrabScreen18;Grab Screen 18;0;0;Create;True;0;0;0;False;0;False;Instance;32;False;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;44;-2843.582,582.4417;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;66;-2614.037,78.49435;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;42;-3204.456,866.7749;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;55;-2409.687,359.0731;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;53;-3027.156,1087.221;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenColorNode;54;-2683.263,582.72;Inherit;False;Global;_GrabScreen21;Grab Screen 21;0;0;Create;True;0;0;0;False;0;False;Instance;32;False;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;52;-2850.059,824.2493;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;73;-3022.173,1008.995;Inherit;False;12;ScreenU;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;4;-2099.011,1259.851;Inherit;False;517;238;normalize brightness;3;8;6;5;;1,1,1,1;0;0
Node;AmplifyShaderEditor.DynamicAppendNode;61;-2853.102,1052.963;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;60;-2284.51,583.4711;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ScreenColorNode;63;-2684.824,819.2497;Inherit;False;Global;_GrabScreen22;Grab Screen 22;0;0;Create;True;0;0;0;False;0;False;Instance;32;False;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;5;-2071.028,1410.014;Inherit;False;Constant;_Float4;Float 4;6;0;Create;True;0;0;0;False;0;False;9;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;67;-2146.741,822.9899;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ScreenColorNode;65;-2685.596,1043.475;Inherit;False;Global;_GrabScreen23;Grab Screen 23;0;0;Create;True;0;0;0;False;0;False;Instance;32;False;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;88;-1517.061,1248.281;Inherit;False;374;310;Color Tint;2;86;87;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;6;-1916.828,1391.453;Inherit;False;2;0;FLOAT;1;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;68;-2036.776,1053.035;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;86;-1499.061,1369.281;Inherit;False;Property;_Tint;Tint;2;0;Create;True;0;0;0;False;0;False;1,1,1,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;8;-1766.029,1308.014;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0.4811321;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;87;-1288.061,1306.281;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;-1072.814,1248.099;Float;False;True;-1;2;ASEMaterialInspector;0;0;Unlit;BlackBook/FX/Box Blur Behind;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;False;Opaque;;AlphaTest;All;16;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;74;0;9;0
WireConnection;78;0;10;0
WireConnection;78;1;74;0
WireConnection;11;0;78;0
WireConnection;15;0;9;2
WireConnection;12;0;9;1
WireConnection;84;0;15;0
WireConnection;16;0;13;0
WireConnection;16;1;14;0
WireConnection;83;1;15;0
WireConnection;83;0;84;0
WireConnection;19;0;16;0
WireConnection;19;1;14;0
WireConnection;29;0;16;0
WireConnection;29;1;17;0
WireConnection;27;0;12;0
WireConnection;27;1;83;0
WireConnection;31;0;19;0
WireConnection;31;1;24;0
WireConnection;40;0;29;0
WireConnection;32;0;27;0
WireConnection;30;0;18;0
WireConnection;30;1;22;0
WireConnection;47;0;30;0
WireConnection;47;1;34;0
WireConnection;51;0;32;0
WireConnection;51;1;40;0
WireConnection;36;0;30;0
WireConnection;36;1;22;0
WireConnection;48;0;31;0
WireConnection;58;0;47;0
WireConnection;59;0;36;0
WireConnection;59;1;49;0
WireConnection;25;0;20;0
WireConnection;25;1;21;0
WireConnection;57;0;51;0
WireConnection;57;1;48;0
WireConnection;35;0;25;0
WireConnection;35;1;21;0
WireConnection;62;0;57;0
WireConnection;62;1;58;0
WireConnection;33;0;70;0
WireConnection;33;1;25;0
WireConnection;64;0;59;0
WireConnection;46;0;33;0
WireConnection;44;0;71;0
WireConnection;44;1;35;0
WireConnection;66;0;62;0
WireConnection;66;1;64;0
WireConnection;42;0;39;0
WireConnection;42;1;41;0
WireConnection;55;0;66;0
WireConnection;55;1;46;0
WireConnection;53;0;42;0
WireConnection;53;1;41;0
WireConnection;54;0;44;0
WireConnection;52;0;72;0
WireConnection;52;1;42;0
WireConnection;61;0;73;0
WireConnection;61;1;53;0
WireConnection;60;0;55;0
WireConnection;60;1;54;0
WireConnection;63;0;52;0
WireConnection;67;0;60;0
WireConnection;67;1;63;0
WireConnection;65;0;61;0
WireConnection;6;1;5;0
WireConnection;68;0;67;0
WireConnection;68;1;65;0
WireConnection;8;0;68;0
WireConnection;8;1;6;0
WireConnection;87;0;8;0
WireConnection;87;1;86;0
WireConnection;0;2;87;0
ASEEND*/
//CHKSM=EA4DCD931FB5572DECC530C482F36772966B4897