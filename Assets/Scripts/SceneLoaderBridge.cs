using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BlackBook.Hidden.Core.ARUX
{
    /// <summary>
    ///Author Mattias Tronslien
    /// For use with back button override 
    /// </summary>
    [AddComponentMenu("Black Book/UI/Load Scene")]
    public class SceneLoaderBridge : MonoBehaviour
    {
        public void LoadSceneByIndex(int index)
        {
            SceneManager.LoadScene(index);
        }
        public void LoadSceneByName(string name)
        {
            SceneManager.LoadScene(name);
        }
        public void OnApplicationQuit()
        {
            Application.Quit();
        }
    }

}