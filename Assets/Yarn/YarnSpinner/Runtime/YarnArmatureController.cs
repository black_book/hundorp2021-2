﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Yarn.Unity {
    public class YarnArmatureController : MonoBehaviour
    {
        //public DragonBones.UnityArmatureComponent armature;
        public string actorName = "Embla";
        public int initialExpression;
        public GameObject[] expressions;

        private static Dictionary<string, YarnArmatureController> actors;
        
        void Awake()
        {
            for (int i = 0; i < expressions.Length; i++)
            {
                expressions[i].SetActive(i==initialExpression);                
            }
        }

        public static YarnArmatureController Find(string actorname)
        {
            if (actors == null) actors = new Dictionary<string, YarnArmatureController>();

            if (actors.ContainsKey(actorname)) return actors[actorname];

            YarnArmatureController[] unsorted = FindObjectsOfType<YarnArmatureController>();
            foreach (YarnArmatureController yac in unsorted)
            {
                actors[yac.actorName] = yac;
            }

            if (actors.ContainsKey(actorname)) return actors[actorname];

            Debug.Log("Could not find actor with name: " + actorname);
            return null;
        }

        public void ChangeExpression(string expression)
        {
            if (expressions == null || expressions.Length == 0) return;

            foreach(GameObject e in expressions)
            {
                e.SetActive(e.name == expression);
            }
        }

        public void ChangeAnimation(string animation)
        {
            Debug.Log("Playing animation " + animation + " on " + actorName);
            //armature.animation.Play(animation);
        }
    }
}
