using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Yarn.Unity
{
    [CreateAssetMenu(fileName = "New Yarn Sound Bank", menuName = "Yarn/New Sound Bank Object")]
    public class YarnSoundBank : ScriptableObject
    {
        public AudioClip[] audioClips;


        /// <summary>
        /// Search the bank for a clip that matches the name given. Returns null if no clip is found.
        /// </summary>
        /// <param name="ameOfClip"></param>
        /// <returns></returns>
        public AudioClip GetClip(string nameOfClip)
        {
            foreach (var clip in audioClips)
            {
                if (clip.name == nameOfClip) return clip;
            }
            return null;
        }

    }
}