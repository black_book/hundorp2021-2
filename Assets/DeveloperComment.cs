using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeveloperComment : MonoBehaviour
{
    [SerializeField,TextArea(5,20), Tooltip("Use this field to leave comments for yourself and other developers. This script has no logic assosiated with it")]
    string comments = "Use this field to leave comments for yourself and other developers.\n";
}
